#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 22 11:26:08 2018

@author: Laurent Gilquin
"""

import numpy as np  # pour creer les plans d'experiences (en particulier)
import scipy.stats as sps  # le module qui contient les lois statistiques
import Saltelli as Salt # module qui implemente la methode de Saltelli
import pandas as pd # module pour les graphiques
import matplotlib as mpl # module pour les graphiques
import matplotlib.pyplot as plt # module pour les graphiques
import matplotlib.patches as mpatches # module pour les graphiques
#%%
"""
Definition de la fonction 'jouet' afin d'illustrer la methode de Saltelli
fonction ishigami : https://www.sfu.ca/~ssurjano/ishigami.html
3 parametres suivant chacun une loi uniforme dans -pi,pi

"""

def ishigami(X):
    
    out = np.sin(X[:,0]) + 7 * np.sin(X[:,1])**2 + 0.1 * X[:,2]**4 * np.sin(X[:,0])
    
    return out
#%%
"""
Construction des deux plans d'experiences X1, X2 recquis par la methode de Saltelli
"""
N = 10000 # nombre d'echantillons (de lignes) de chacun des deux plans X1, X2
d = 3 # nombres de parametres
rv = sps.uniform(loc=-np.pi, scale = 2*np.pi) # creation de la loi uniform entre -pi et pi
X1 = rv.rvs(N * d).reshape(N, d) # creation de X1
X2 = rv.rvs(N * d).reshape(N, d) # creation de X2
#%%
"""
Construction du plans d'experiences contenant les jeux de parametres avec lesquels le modele doit etre 
evalue : matrice de taille N(d+2) * d
"""
AS = Salt.Saltelli() # creation d'une instance Saltelli
AS.generate_DoE(None, X1, X2, scheme="A", nboot=100) # appel a la methode construisant le plan d'experience
# nboot specifie le nombre d'echantillons bootstrap pour construire les intervalles de confiance des indices
# de Sobol'
Param = AS.design # assignation du plan

"""
P.S : a cette etape la, il est utile de sauvegarder l'objet Salt, ca peut se faire en via pickle :
import pickle
pklfile = open("/path_to_save/name_file.pkl", 'wb')
pickle.dump(AS, pklfile)
pklfile.close()
"""
#%%
"""
Evaluation du modele et calcul des indices de Sobol'
"""
y = ishigami(Param) # evaluations du modele
AS.tell(y) # evaluation des indices
valS = AS.S # indices d'ordre un, la premier colonne contient les estimations, les deuxiemes et troisiemes
# colonnes contiennent les bornes de l'intervalle de confiance
valT = AS.T # indices d'ordre un, la premier colonne contient les estimations, les deuxiemes et troisiemes
# colonnes contiennent les bornes de l'intervalle de confiance

print(valS, valT)

"""
P.S : si besoin de charger un objet sauvegarder avec pickle :
import pickle
pklfile = open("/path_to_save/name_file.pkl", 'rb')
AS = pickle.load(pklfile)
pklfile.close()
"""
#%%
"""
Cas sortie vectorielle
"""
bigY = np.array([y * sps.norm.rvs(size=y.size, loc=t, scale=1.0/(t+1)) for t in range(10)]).T # creation naive de 10 vecteurs reponses pour
# simuler une sortie vectorielle, on obtient un objet de taille N(d+2) * 10
valS = np.zeros((d, 3, bigY.shape[1]))
valT = np.zeros((d, 3, bigY.shape[1]))
for it in range(bigY.shape[1]): # boucle sur les vecteurs de reponses (colonnes de bigY)
    AS.tell(bigY[:,it]) # evaluation des indices
    valS[...,it] = AS.S # stockage des estimations + intervalles de confiance pour les indices d'ordre un
    valT[...,it] = AS.T # stockage des estimations + intervalles de confiance pour les indices totaux
#%%
"""
Graphiques panda
"""
cmap = mpl.cm.get_cmap('PuBuGn') # choix du colormap

table = valT[:,0,:].T # recuperation des estimations des indices d'ordre un
table[table<0] = 0.0 # on met a zero les valeurs negatives (les indices sont censes etre positifs)
df = pd.DataFrame(table) # construction du data.frame
ax = df.plot(kind='area', stacked =True, cmap = cmap, legend=True, title=False) # creation du stacked area chart
ax.grid(color='black', linestyle='-', linewidth=1, alpha=0.2) # ajout d'une grille en background
leg = ax.legend_ # creation de la legende
# modification des labels des ticks de l'axe des abscisse
labels = (ax.get_xticks()+1)*100 
labels = labels.astype(np.int)
ax.set_xticklabels(labels)
# modification du titre de l'axe des abscisse
ax.set_xlabel("temps", color='black')
# changer la legende
X1_patch = mpatches.Patch(color=leg.legendHandles[0].get_facecolor(), label=r'$x_1$')
X2_patch = mpatches.Patch(color=leg.legendHandles[1].get_facecolor(), label=r'$x_2$')
X3_patch = mpatches.Patch(color=leg.legendHandles[2].get_facecolor(), label=r'$x_3$')
leg = plt.legend(handles=[X1_patch, X2_patch, X3_patch], loc='bottom right')
leg.get_frame().set_edgecolor('black')
leg.get_frame().set_facecolor('white')
fig = plt.gcf() # recuperer la figure courante
fig.set_size_inches(8, 6) # changer les dimensions de la figure
# commande pour sauver la figure en pdf avec fond transparent et bords rognes
# plt.savefig("path_to_save/name_file.pdf", dpi=200, bbox_inches='tight', transparent=True)
plt.show()
