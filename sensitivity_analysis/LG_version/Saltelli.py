# -*- coding: utf-8 -*-
from __future__ import division, print_function
"""
Created on Thu May 11 14:34:14 2017

@author: Laurent
"""

import types
import numpy as np
import scipy.stats as ssp
from LG_estimator import LG_estimator


def combn(n):
    """
    Generate all unordered combinations of two elements of the array [0,...,n-1].

    Parameters
    ----------
    n : int
    length of the array.

    Returns
    -------
    out : ndarray
        2-D array of shape (p(p-1)/2, 2) containing the combinations.

    Examples
    --------
    >>> combn(4)
    array([[0, 1],
           [0, 2],
           [0, 3],           
           [1, 2],
           [1, 3],
           [2, 3]])

    """
    i, j = np.triu_indices(n, 1)
    out = np.stack([i,j],axis=1)

    return out


class Saltelli():
    
    
    
    def generate_DoE(self, model, X1, X2, groups = None, scheme = 'A', nboot = 0, conf = 0.95):
        
        
        # conditions checking 
        assert np.any(isinstance(model, types.FunctionType) or model is None), u"model must be either a function or None"
        assert X1.__class__ == np.ndarray, u"X1 must be a numpy.array"
        assert X2.__class__ == np.ndarray, u"X2 must be a numpy.array"       
        assert np.all(X1.shape == X2.shape), u"X1 and X2 must have same shape"
        assert np.any(scheme =='A' or scheme =='B'), u"scheme must be a string, either A or B"


        # factor resizing
        if groups is not None:
            
            if scheme == 'B':
                raise Exception('currently not implemented')
            else:
                assert type(groups) == dict, u"groups must be a dict"
                n, p = X1.shape
                nb_group_inputs = np.sum(groups['size'])
                nb_groups = groups['number']
                newp = p - nb_group_inputs + nb_groups
            
        else:
            n, p = X1.shape
            newp = p
            nb_group_inputs = 0
        
        # construction of the Design of Experiments (DoE)
        s = np.arange(1, p - nb_group_inputs + 1)
        I1 = (p+1)*s-1
        
        if scheme=='A':
            
            X0 = np.concatenate([X1,np.tile(X2,newp+1)],axis=1)
            #ungrouped inputs
            X0[:,I1] = X1[:,s-1]
            
            if groups is not None:
                #grouped inputs
                newsize = np.r_[1,groups['size']]
                cum_nele = np.cumsum(newsize)[:-1]
                
                for it in range(groups['number']):
                    pos = p - nb_group_inputs + cum_nele[it] - 1
                    nele = newsize[it+1]
                    start = p * (s[-1] + it + 1) + pos
                    X0[:,start:(start+nele)] = X1[:,(pos):(pos+nele)]
            
            index = (np.arange(p)[:,None]+np.arange(0,(newp+2)*p,p)).flatten()
            D = ((X0.T)[index,:]).flatten()
            D = np.reshape(D,(p,n*(newp+2))).T
        
        
        #TODO : grouped version for scheme B
        
        if scheme=='B':  
            
            X0 = np.concatenate([X1,np.tile(X2,2*p+1)],axis=1)
            
            mat = np.zeros((p,p-1),dtype=np.int)
            mat[np.tril_indices(mat.shape[0],-1)] = -1
            mat = mat + np.arange(p)[1:]
            I2 = np.arange((p+1)*p,2*p**2+1,p)[:,None] + mat
            I = np.concatenate([I1,I2.flatten()])
            
            X0[:,I] = X1[:,np.concatenate([s-1,mat.flatten()])]
            
            index = (np.arange(p)[:,None]+np.arange(0,(2*p+2)*p,p)).flatten()
            D = ((X0.T)[index,:]).flatten()
            D = np.reshape(D,(p,n*(2*p+2))).T

            
        # stocking attributes
        self.scheme = scheme
        self.nboot = nboot
        self.conf = conf
        self.design = D
        self.n = n
        self.dim = newp


        if model is not None:
            
            self.response()
            self.tell()
    
    
    def bootstrap(self):
        """
        Returns bootstrap estimate of 100.0*(1-alpha) CI for statistic.
        """
    
        alpha = 1-self.conf
        n, d = self.y.shape
        num_samples = self.nboot
    
        idx = np.random.randint(0, n, (num_samples, n))
    
        if self.scheme == "A":
            p = d-2
            booted = np.zeros((num_samples,np.int(2*p)))
        else:
            p = (d-2)/2
            booted = np.zeros((num_samples,np.int(2*p+p*(p-1)/2)))
    
        #get the boostrapped results 
        for it in range(num_samples):
        
            samples = self.y[idx[it,:],:]
            booted[it,:] = self.estimate(samples)
    
        # construction of the CI (type classic bias corrected)
        booted_mean = np.mean(booted,axis=0)
        z0_hat = ssp.norm.ppf(np.sum(booted<=booted_mean,axis=0)/np.float64(num_samples))
        modif_probs = ssp.norm.cdf(2*z0_hat[:,None]+ssp.norm.ppf([alpha/2.0,1-alpha/2.0]))
        Out = np.zeros((booted.shape[1],3))
        Out[:,0] = booted_mean
        for j in range(Out.shape[0]):
            Out[j,1] = np.percentile(booted[:,j], 100*modif_probs[j,0])
            Out[j,2] = np.percentile(booted[:,j], 100*modif_probs[j,1])
        
        return Out
    
    
    
    
    def response(self, loop=False):
        
        if loop:
            
            y = [self.model(x) for x in self.design] #note model must return a np.array!
            self.y = np.stack(y,axis=0)
            
        else:
            
            self.y = self.model(self.X)
    
    

    
    def estimate(self, data=None):
        
        if data is None:
            
            data = self.y
        
        
        if self.scheme == "A":
            
            #quantity of interest
            p = self.dim
            Mean = np.nanmean(data, axis=0)
            MeanY2 = (Mean[0]+Mean[1:(p+1)])/2
            MeanY2t = (Mean[p+1]+Mean[1:(p+1)])/2
            
            #replace nan values
            data[np.isnan(data)] = -1000.0
            
            #first order Sobol' indices
            ind1 = np.repeat(0,p)
            ind2 = np.linspace(1, p, p, dtype=np.int64)
            S = LG_estimator(data, MeanY2, ind1, ind2, p)
            
            #total effect Sobol' indices
            ind1 = np.repeat(p+1, p)
            ind2 = np.linspace(1, p, p, dtype=np.int64)
            St = 1-LG_estimator(data, MeanY2t, ind1, ind2, p)
            
            Out = np.concatenate((S,St))
            
        
        if self.scheme == "B":
            
            #quantity of interest
            p = self.dim
            Mean = np.nanmean(data, axis=0)
            MeanY2 = (Mean[0]+Mean[1:(p+1)])/2
            MeanY2t = (Mean[2*p+1]+Mean[1:(p+1)])/2
            MeanY3 = (Mean[2*p+1]+Mean[(p+1):(2*p+1)])/2
            MeanY3t = (Mean[0]+Mean[(p+1):(2*p+1)])/2
            
            #replace nan values
            data[np.isnan(data)] = -1000.0
            
            #first order Sobol' indices
            ind1 = np.repeat(0, p)
            ind2 = np.linspace(1, p, p, dtype=np.int64)
            S_i = LG_estimator(data, MeanY2, ind1, ind2, p)
            ind1 = np.repeat(2*p+1, p)
            ind2 = np.linspace(p+1, 2*p, p, dtype=np.int64)
            S_ii = LG_estimator(data, MeanY3, ind1, ind2, p)
            S = (S_i+S_ii)/2
            
            #total effect Sobol' indices
            ind1 = np.repeat(2*p+1, p)
            ind2 = np.linspace(1, p, p, dtype=np.int64)
            St_i = LG_estimator(data, MeanY2t, ind1, ind2, p)
            ind1 = np.repeat(0, p)
            ind2 = np.linspace(p+1, 2*p, p, dtype=np.int64)
            St_ii = LG_estimator(data, MeanY3t, ind1, ind2, p)
            St = 1-(St_i+St_ii)/2
                 
            #second-order Sobol' indices (make use of double estimates)
            I = combn(p)
            nb_S = I.shape[0]
            MeanY4 = (Mean[1+I[:,0]]+Mean[p+1+I[:,1]])/2
            MeanY4t = (Mean[1+I[:,1]]+Mean[p+1+I[:,0]])/2
            
                      
            ind1 = 1+I[:,0]
            ind2 = p+1+I[:,1]
            S2_i = LG_estimator(data, MeanY4, ind1, ind2, nb_S)
            ind1 = 1+I[:,1]
            ind2 = p+1+I[:,0]
            S2_ii = LG_estimator(data, MeanY4t, ind1, ind2, nb_S)
            S2 = (S2_i+S2_ii)/2
            S2 = S2-S[I[:,0]]-S[I[:,1]]
            
            Out = np.concatenate((S,St,S2))
                 
            
        return Out
    
        

    
    def tell(self, y=None):
        
        if y is not None:
            
            self.y = y
                    
        n = self.n
        p = self.dim
        
        self.y = np.reshape(self.y, (-1,n)).T
        self.V = np.var(self.y, ddof=1)
        
        #TODO : output purpose
        
        # indices estimation without bootstrap
        if self.nboot == 0:
            
            indices = self.estimate()
            self.S  = indices[:p]
            self.T  = indices[p:(2*p)]
            
            if self.scheme == "B":
                self.S2 = indices[(2*p):]
                
        # indices estimation witho bootstrap
        else:
            
            indices = self.bootstrap()
            self.S = indices[:p,:]
            self.T = indices[p:(2*p),:]

            if self.scheme == "B":
                self.S2 = indices[(2*p):,:]
