#!/usr/bin/env python
import sys
import os

if len(sys.argv) < 2:
    print('usage: format_indices.py <Sobol\' indices path>')
sobol_indices_path = os.path.join(os.getcwd(), sys.argv[1])

files = [int(f[:-4]) for f in os.listdir(sobol_indices_path) if os.path.isfile(os.path.join(sobol_indices_path, f)) and f.endswith('.pkl')]
files = list(map(lambda i: str(i)+'.pkl', sorted(files)))

import pickle as pkl

indices_sep = [None] * len(files)

for i, f in enumerate(files):
    with open(os.path.join(sobol_indices_path, f), 'rb') as f:
        indices_sep[i] = pkl.load(f)
    print(indices_sep[i].shape)
import numpy as np

print(np.concatenate(indices_sep, 1).shape)
