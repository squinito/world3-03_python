#!/usr/bin/env python

if __name__ == "__main__":
    import sys

    if len(sys.argv) < 4 or not (sys.argv[2].isnumeric() and sys.argv[3].isnumeric()):
        print('usage: init.py <world3.py> <N> <Nw>')
        print('N is the Saltelli parameter')
        print('Nw is the number of workers')
        exit(1)
    else:
        w3_py_path = sys.argv[1]
        N = int(sys.argv[2])
        num_workers = int(sys.argv[3])
        print(f'Initializing World3 sensitivity analysis with N = {N} and {num_workers} workers...')
        
    import pysd
    import json
    import os
    import stat
    import numpy as np
    from SALib.sample import saltelli

    current_path = os.path.realpath(os.path.dirname(os.path.abspath(__file__)))

    # Some configuration
    result_dir_name = 'res'
    indices_dir_name = 'indices'
    param_values_dir_name = 'param_values'
    param_values_filename_template = 'param_values_{}.pkl'
    results_filename_template = 'stocks_{}'
    immuables_file_path = os.path.join(current_path, 'immuables.json')

    result_dir_path = os.path.normpath(os.path.join(current_path, result_dir_name))
    indices_dir_path = os.path.normpath(os.path.join(current_path, indices_dir_name))
    param_values_path = os.path.normpath(os.path.join(current_path, param_values_dir_name))
    param_values_file_path_template = os.path.normpath(os.path.join(param_values_path, param_values_filename_template))
    results_file_path_template = os.path.normpath(os.path.join(result_dir_path, results_filename_template))

    # load all variables list
    w3 = pysd.load(w3_py_path)
    variables = {row['Real Name']: dict(row) for index, row in w3.doc().iterrows()}

    # load immuables variables (constants that won't change, like start and end times)
    with open(immuables_file_path, 'r') as f:
        immuables = json.load(f)

    # list constants to drop them from stocks of w3 runs
    constants = [
        k for k, v in variables.items()
        if v['Type'] == 'constant'
    ] + ['TIME']

    # generate list of names of variables we want to test
    py_names = [
        v['Py Name'] for k, v in variables.items()
        if v['Type'] == 'constant'
        and v['Py Name'] not in immuables
    ]

    # Generate -10%, +10% bounds
    bounds = [
        [
            getattr(w3.components, c)() * i
            for i in [.9, 1.1]
        ]
        for c in py_names
    ]

    num_vars = len(py_names)
    print('Number of variables:', num_vars)

    print('Run W3 once to get the output scheme...')
    w3_stocks = w3.run()
    components = list(w3_stocks.drop(columns = constants).columns.values)
    print(
        'Number of components w/o consts in output:',
        len(components),
        '/',
        len(w3_stocks.columns.values)
    )

    problem = {
        'num_vars': num_vars,
        'names': py_names,
        'bounds': bounds,
        'components': components,
        'results_file_path_template': results_file_path_template,
        'world3_path': w3_py_path,
        'indices_dir_path': indices_dir_path
    }

    print('Generating samples...')
    param_values = saltelli.sample(problem, N, calc_second_order = False)
    num_runs = len(param_values)
    print('Total number of runs:', num_runs)

    param_values_w_labels = np.array([dict(zip(problem['names'], p)) for p in param_values])
    param_values_w_labels_split = np.array_split(param_values_w_labels, num_workers)

    def create_dir(path):
        if not os.path.isdir(path):
            if os.path.isfile(path):
                os.remove(path)
            os.mkdir(path)

    print('Creating', param_values_path, 'dir...')
    create_dir(param_values_path)

    print('Creating', result_dir_path, 'dir...')
    create_dir(result_dir_path)

    print('Creating', indices_dir_path, 'dir...')
    create_dir(indices_dir_path)

    for worker_id in range(num_workers):
        path = param_values_file_path_template.format(worker_id)
        print(
            'Saving',
            len(param_values_w_labels_split[worker_id]),
            'runs in',
            os.path.basename(path),
            end = '...\n'
        )
        param_values_w_labels_split[worker_id].dump(path)
        
    print('Saving the SA parameters with additionnal informations in problem.json...')

    problem['num_runs'] = num_runs
    with open('problem.json', 'w') as f:
        json.dump(problem, f)

    exec_runs_file_content = f'''#!/usr/local/bin/bash -l

#$ -P P_liris       # SPS liris
#$ -j y             # redirect stderr in stdout
#$ -l os='cl7'      # Linux
#$ -N World3_runs   # Job name
#$ -V               # export env
#$ -l sps=1
#$ -t 1-{num_workers}:1

#$ -q long

echo "Task id = $SGE_TASK_ID"

echo "Job start: `date`"
W3_SA_PATH="{current_path}"
WORKER_ID=$((SGE_TASK_ID - 1))
INPUT_FILENAME="{param_values_filename_template.format('${WORKER_ID}')}"
INPUT="${{W3_SA_PATH}}/{param_values_dir_name}/${{INPUT_FILENAME}}"
SCRIPT_PATH="${{W3_SA_PATH}}/run.py"

RUN_TMP_FILE=${{TMPDIR}}/runs_tmp
OUTPUT_FILE="${{W3_SA_PATH}}/{result_dir_name}/{results_filename_template.format('${WORKER_ID}')}"

# Run the simulation
${{SCRIPT_PATH}} ${{INPUT}} ${{RUN_TMP_FILE}}

echo "Copies"
cp -v ${{RUN_TMP_FILE}} ${{OUTPUT_FILE}}

echo "Job end: `date`"'''

    exec_analysis_file_content = f'''#!/usr/local/bin/bash -l

#$ -P P_liris                # SPS liris
#$ -j y                      # redirect stderr in stdout
#$ -l os='cl7'               # Linux
#$ -N World3_analysis        # Job name
#$ -V                        # export env
#$ -l sps=1

#$ -q mc_highmem_long

STEP_BEG=$1
STEP_END=$2

echo "Job start: `date`"
W3_SA_PATH="{current_path}"
SCRIPT_PATH="${{W3_SA_PATH}}/analyze.py"
SHM_PATH=/dev/shm

# Initialization
echo "Cleaning"
rm -f ${{SHM_PATH}}/*

echo "Copying"
cp -v ${{INPUT}}/* ${{SHM_PATH}}

# Run the simulation
${{SCRIPT_PATH}} ${{STEP_BEG}} ${{STEP_END}}

echo "Job end: `date`"'''

    def create_sh_file(filename, file_content):
        print(f'Writing {filename}...')
        file_path = os.path.join(current_path, filename)
        if os.path.isfile(file_path): os.remove(file_path)
        with open(file_path, 'w') as f:
            f.write(file_content)
        print(f'Changing permissions of {filename}...')
        os.chmod(file_path, stat.S_IRUSR | stat.S_IWUSR | stat.S_IEXEC | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)
    
    create_sh_file('exec_runs.sh', exec_runs_file_content)
    create_sh_file('exec_analysis.sh', exec_analysis_file_content)

    print('Done!')
    
    print('''
TODO now:
$ qsub exec_runs.sh

After the end of all the jobs, run the sensitivity analysis, splitting the time steps to distribute on several workers.
Example (3 workers):
$ qsub exec_analysis.sh 0 133
$ qsub exec_analysis.sh 133 266
$ qsub exec_analysis.sh 266 401

Example with a loop (20 workers):
$ for i in {0..8}; do qsub exec_analysis.sh $((i * 40)) $(((i + 1) * 40)); done
$ exec_SA.sh 360 401

NB: 401 must be the last step.''')
