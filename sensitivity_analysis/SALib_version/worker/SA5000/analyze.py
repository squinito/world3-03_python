#!/usr/bin/env python
import sys

# Définition du pas de temps à prendre
if len(sys.argv) < 3:
    print('usage: analyze.py <start> <end>')
    sys.exit()
else:
    step_beg = int(sys.argv[1])
    step_end = int(sys.argv[2])

import struct
import time
import os
import json
import psutil
import numpy as np
from SALib.analyze import sobol

def display(*args, **kwargs):
    sep = kwargs['sep'] if 'sep' in kwargs else ' '
    print('[PY ' + time.ctime() + '] ' + sep.join(map(str, args)), **kwargs)
    sys.stdout.flush()

with open('problem.json', 'r') as problem_file:
    problem = json.load(problem_file)

input_file_path_template = problem['results_file_path_template']
indices_dir_path = problem['indices_dir_path']
num_runs = problem['num_runs']

indices_path = os.path.join(indices_dir_path, str(step_beg))


'''
Initialization
'''
num_columns = len(problem['components'])
num_steps = step_end - step_beg
num_steps_total = 401
run_size = 8 * num_steps_total * num_columns
display("Number of columns: ", num_columns)

runs = []
process = psutil.Process(os.getpid())

all_runs = np.ndarray((num_runs, num_steps, num_columns))

run_no_total = 0

display('Checking files...')
i = 0
while True:
    file = input_file_path_template.format(i)
    i += 1
    if not os.path.exists(file):
        break
    num_runs = os.stat(file).st_size / run_size
    if not num_runs.is_integer() or num_runs == 0:
        raise Exception('Incorrect file size for ' + file)
    display('Number of runs in', file, ':', int(num_runs))

display('Reading files...')
i = 0
while True:
    file = input_file_path_template.format(i)
    i += 1
    if not os.path.exists(file):
        break

    num_runs = int(os.stat(file).st_size / run_size)

    start_time = time.time()
    run_file = open(file, 'rb')

    '''
    Reading all runs values from all files for all time steps
    '''
    for run_no in range(num_runs):
        for time_step in range(num_steps):
            real_time_step = step_beg + time_step
            run_offset = run_size * run_no
            inner_offset = num_columns * real_time_step * 8
            offset = run_offset + inner_offset
            values = []
            run_file.seek(offset)
            for v in range(num_columns):
                value = struct.unpack('d', run_file.read(8))[0]
                all_runs[run_no_total, time_step, v] = value
        run_no_total += 1

    run_file.close()
    display('Time:', time.time() - start_time)
    display('RAM usage', process.memory_info().rss)

display('Shape of all runs:', all_runs.shape)
display('Number of runs loaded:', run_no_total)

# vector_no (output) ; time_step ; indices and incertitudes ; num vars (input)
Si = np.ndarray((num_columns, num_steps, 4, problem['num_vars']))

display('Analyzing...')
np.seterr(all='ignore')
# iterate over time steps
for time_step in range(num_steps):
    display('Time step', time_step)
    
    start_time = time.time()

    for v in range(num_columns):
        try:
            Si_b = np.stack(sobol.analyze(problem, all_runs[:, time_step, v], calc_second_order = False).values())
            Si[v, time_step, ...] = Si_b
        except ZeroDivisionError:
            display('ZeroDivisionError v=', v)

display('Saving analysis...')
Si.dump(indices_path)

