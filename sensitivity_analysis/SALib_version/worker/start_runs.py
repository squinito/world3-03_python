#!/usr/bin/env python
from config_runs import *
from time import time
from sys import argv, exit
import os, sys
import signal
import struct

print('[PY] Runs init')
sys.stdout.flush()

input_file = argv[1]
scratch_file = argv[2]

print('[PY] scratch_file:', scratch_file)
print('[PY] Loading input set', input_file)
sys.stdout.flush()
inputs_set = np.load(input_file)

flush_no = 0

# Sauvegarde à chaque signal (on ne peut pas handle le SIGKILL)
def signal_handler(signal, frame):
    print('\n[PY] Handling signal', signal)
    display_stats()
    exit(0)

signals = ['SIGINT', 'SIGUSR1', 'SIGUSR2', 'SIGTERM', 'SIGXCPU', 'SIGXFSZ']
for s in signals:
    print('[PY]', end = ' ')
    try:
        signal.signal(getattr(signal, s), signal_handler)
        print('Signal', s, 'OK')
    except:
        print('Signal', s, 'No OK')

def display_stats():
    global total_runs_acc, inputs_set, start_time
    current_time = time()
    print(f'''
==============
Stats:
total runs   : {total_runs_acc} / {len(inputs_set)}
time elapsed : {current_time - start_time}
avg per run  : {(current_time - start_time) / total_runs_acc}
==============
''')
    sys.stdout.flush()

# "Flush" dans le cas d'un fichier : on append à chaque itération en local
def append_stocks_to_file(stocks, run_file):
    for index, row in stocks.iterrows():
        for v in row:
            run_file.write(struct.pack('d', v))


run_file = open(scratch_file, 'wb')

print('[PY] Saving metadata')
sys.stdout.flush()
stocks = w3.run(inputs_set[0]).drop(columns = const_params)
meta_file = open(scratch_file + '_meta', 'w')
meta_file.write("\n".join(stocks))
meta_file.close()

print('[PY] Runs start! Total:', len(inputs_set), 'runs')
sys.stdout.flush()
total_runs_acc = 0

start_time = time()
for inputs_id in range(len(inputs_set)):
    stocks = w3.run(inputs_set[inputs_id]).drop(columns = const_params)
    total_runs_acc += 1
    append_stocks_to_file(stocks, run_file)
run_file.close()
print('[PY] Runs end!')
display_stats()
sys.stdout.flush()
