@cache('step')
def crowding_multiplier_from_industry():
    """
    crowding multiplier from industry

    Dmnl

    component

    CROWDING MULTIPLIER FROM INDUSTRIALIZATION (CMI#27).
    """
    return crowding_multiplier_from_industry_table(industrial_output_per_capita() / gdp_pc_unit())


def crowding_multiplier_from_industry_table(x):
    """
    crowding multiplier from industry table

    Dmnl

    lookup

    Table relating industrial output to crowding (CMIT#27.1).
    """
    return functions.lookup(x, [0, 200, 400, 600, 800, 1000, 1200, 1400, 1600],
                            [0.5, 0.05, -0.1, -0.08, -0.02, 0.05, 0.1, 0.15, 0.2])


@cache('step')
def death_rate():
    """
    death rate

    C/year

    component

    CRUDE DEATH RATE (CDR#18)
    """
    return thousand() * deaths() / population()


@cache('step')
def deaths():
    """
    deaths

    Person/year

    component

    The total number of deaths per year for all age groups (D#17).
    """
    return deaths_0_to_14() + deaths_15_to_44() + deaths_45_to_64() + deaths_65_plus()


@cache('step')
def effective_health_services_per_capita():
    """
    effective health services per capita

    $/(Person*year)

    component

    Effective health services per capita - delayed from allocation (EHSPC#22)
    """
    return smooth_health_services_per_capita_health_services_impact_delay_health_services_per_capita_1()

smooth_health_services_per_capita_health_services_impact_delay_health_services_per_capita_1 = functions.Smooth(
    lambda: health_services_per_capita(), lambda: health_services_impact_delay(),
    lambda: health_services_per_capita(), lambda: 1)

@cache('step')
def fraction_of_population_urban():
    """
    fraction of population urban

    Dmnl

    component

    FRACTION OF POPULATION URBAN (FPU#26).
    """
    return fraction_of_population_urban_table(population() / unit_population())


def fraction_of_population_urban_table(x):
    """
    fraction of population urban table

    Dmnl

    lookup

    Table relating population to the fraction of population that is urban (FPUT#26.1).
    """
    return functions.lookup(x, [0, 2e+09, 4e+09, 6e+09, 8e+09, 1e+10, 1.2e+10, 1.4e+10, 1.6e+10],
                            [0, 0.2, 0.4, 0.5, 0.58, 0.65, 0.72, 0.78, 0.8])


@cache('step')
def health_services_per_capita():
    """
    health services per capita

    $/(Person*year)

    component

    Health services allocation per capita (HSAPC#21).
    """
    return health_services_per_capita_table(service_output_per_capita() / gdp_pc_unit())


def health_services_per_capita_table(x):
    """
    health services per capita table

    $/(Person*year)

    lookup

    The table relating service output to health services (HSAPCT#21.1).
    """
    return functions.lookup(x, [0, 250, 500, 750, 1000, 1250, 1500, 1750, 2000],
                            [0, 20, 50, 95, 140, 175, 200, 220, 230])


@cache('run')
def health_services_impact_delay():
    """
    health services impact delay

    year

    constant

    The delay between allocating health services, and realizing the benefit (HSID#22.1).
    """
    return 20


@cache('run')
def life_expectancy_normal():
    """
    life expectancy normal

    year

    constant

    The normal life expectancy with subsistance food, no medical care and no industrialization (LEN#19.1)
    """
    return 28


@cache('step')
def life_expectancy():
    """
    life expectancy

    year

    component

    The average life expectancy (LE#19).
    """
    return life_expectancy_normal() * lifetime_multiplier_from_food(
    ) * lifetime_multiplier_from_health_services() * lifetime_multiplier_from_persistent_pollution(
    ) * lifetime_multiplier_from_crowding()


@cache('step')
def lifetime_multiplier_from_crowding():
    """
    lifetime multiplier from crowding

    Dmnl

    component

    LIFETIME MULTIPLIER FROM CROWDING (LMC#28)
    """
    return 1 - (crowding_multiplier_from_industry() * fraction_of_population_urban())


@cache('step')
def lifetime_multiplier_from_food():
    """
    lifetime multiplier from food

    Dmnl

    component

    The life expectancy multiplier from food (LMF#20)
    """
    return lifetime_multiplier_from_food_table(food_per_capita() / subsistence_food_per_capita())


def lifetime_multiplier_from_food_table(x):
    """
    lifetime multiplier from food table

    Dmnl

    lookup

    The table ralating relative food to the life expectancy multiplier for food (LMFT#20.1)
    """
    return functions.lookup(x, [0, 1, 2, 3, 4, 5], [0, 1, 1.43, 1.5, 1.5, 1.5])


@cache('step')
def lifetime_multiplier_from_health_services():
    """
    lifetime multiplier from health services

    Dmnl

    component

    The life expectancy multiplier from health services (LMHS#23).
    """
    return functions.if_then_else(time() > 1940, lifetime_multiplier_from_health_services_2(),
                                  lifetime_multiplier_from_health_services_1())


@cache('step')
def lifetime_multiplier_from_health_services_1():
    """
    lifetime multiplier from health services 1

    Dmnl

    component

    The life expectancy multiplier from health services before 1940 (LMHS1#24).
    """
    return lifetime_multiplier_from_health_services_1_table(
        effective_health_services_per_capita() / gdp_pc_unit())


def lifetime_multiplier_from_health_services_1_table(x):
    """
    lifetime multiplier from health services 1 table

    Dmnl

    lookup

    Table relating effective health care to life expectancy (LMHS1T#24.1).
    """
    return functions.lookup(x, [0, 20, 40, 60, 80, 100], [1, 1.1, 1.4, 1.6, 1.7, 1.8])


@cache('step')
def lifetime_multiplier_from_health_services_2():
    """
    lifetime multiplier from health services 2

    Dmnl

    component

    The life expectancy multipier from health services value after 1940 (LMHS2#25).
    """
    return lifetime_multiplier_from_health_services_2_table(
        effective_health_services_per_capita() / gdp_pc_unit())


def lifetime_multiplier_from_health_services_2_table(x):
    """
    lifetime multiplier from health services 2 table

    Dmnl

    lookup

    Table relating effective health care to life expectancy (LMHS2T#25.1)
    """
    return functions.lookup(x, [0, 20, 40, 60, 80, 100], [1, 1.5, 1.9, 2, 2, 2])


@cache('step')
def lifetime_multiplier_from_persistent_pollution():
    """
    lifetime multiplier from persistent pollution

    Dmnl

    component

    LIFETIME MULTIPLIER FROM PERSISTENT POLLUTION (LMP#29)
    """
    return lifetime_multiplier_from_persistent_pollution_table(persistent_pollution_index())


def lifetime_multiplier_from_persistent_pollution_table(x):
    """
    lifetime multiplier from persistent pollution table

    Dmnl

    lookup

    Table relating persistent pollution to life expectancy (LMPT#29.1)
    """
    return functions.lookup(x, [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
                            [1, 0.99, 0.97, 0.95, 0.9, 0.85, 0.75, 0.65, 0.55, 0.4, 0.2])


