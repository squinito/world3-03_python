@cache('step')
def average_industrial_output_per_capita():
    """
    average industrial output per capita

    $/(Person*year)

    component

    Average industrial output per capita (AIOPC#43).
    """
    return smooth_industrial_output_per_capita_income_expectation_averaging_time_industrial_output_per_capita_1()

smooth_industrial_output_per_capita_income_expectation_averaging_time_industrial_output_per_capita_1 = functions.Smooth(
    lambda: industrial_output_per_capita(), lambda: income_expectation_averaging_time(),
    lambda: industrial_output_per_capita(), lambda: 1)


@cache('step')
def birth_rate():
    """
    birth rate

    C/year

    component

    The crude birth rate measured in people per thousand people per year (CBR#31).
    """
    return thousand() * births() / population()


@cache('step')
def births():
    """
    births

    Person/year

    component

    The total number of births in the world (B#30).
    """
    return functions.if_then_else(
        time() >= population_equilibrium_time(), deaths(),
        (total_fertility() * population_15_to_44() * 0.5 / reproductive_lifetime()))


@cache('step')
def completed_multiplier_from_perceived_lifetime():
    """
    completed multiplier from perceived lifetime

    Dmnl

    component

    COMPENSATORY MULTIPLIER FROM PERCEIVED LIFE EXPECTANCY (CMPLE#36).
    """
    return completed_multiplier_from_perceived_lifetime_table(
        perceived_life_expectancy() / one_year())


def completed_multiplier_from_perceived_lifetime_table(x):
    """
    completed multiplier from perceived lifetime table

    Dmnl

    lookup

    Table relating perceived life expectancy to birth rate compensation (CMPLET#36.1).
    """
    return functions.lookup(x, [0, 10, 20, 30, 40, 50, 60, 70, 80],
                            [3, 2.1, 1.6, 1.4, 1.3, 1.2, 1.1, 1.05, 1])


@cache('step')
def delayed_industrial_output_per_capita():
    """
    delayed industrial output per capita

    $/(Person*year)

    component

    Delayed industrial output per capita (DIOPC#40).
    """
    return smooth_industrial_output_per_capita_social_adjustment_delay_industrial_output_per_capita_3()

smooth_industrial_output_per_capita_social_adjustment_delay_industrial_output_per_capita_3 = functions.Smooth(
    lambda: industrial_output_per_capita(), lambda: social_adjustment_delay(),
    lambda: industrial_output_per_capita(), lambda: 3)

@cache('step')
def desired_completed_family_size():
    """
    desired completed family size

    Dmnl

    component

    Desired completed family size (DCFS#38)
    """
    return functions.if_then_else(
        time() >= zero_population_growth_time(), 2,
        desired_completed_family_size_normal() * family_response_to_social_norm() *
        social_family_size_normal())


@cache('run')
def desired_completed_family_size_normal():
    """
    desired completed family size normal

    Dmnl

    constant

    DESIRED COMPLETED FAMILY SIZE NORMAL (DCFSN#38.2).
    """
    return 3.8


@cache('step')
def desired_total_fertility():
    """
    desired total fertility

    Dmnl

    component

    DESIRED TOTAL FERTILITY (DTF#35).
    """
    return desired_completed_family_size() * completed_multiplier_from_perceived_lifetime()


@cache('step')
def family_income_expectation():
    """
    family income expectation

    Dmnl

    component

    Family income expectations (FIE#42).
    """
    return (industrial_output_per_capita() -
            average_industrial_output_per_capita()) / average_industrial_output_per_capita()


@cache('step')
def family_response_to_social_norm():
    """
    family response to social norm

    Dmnl

    component

    FAMILY RESPONSE TO SOCIAL NORM (FRSN#41).
    """
    return family_response_to_social_norm_table(family_income_expectation())


def family_response_to_social_norm_table(x):
    """
    family response to social norm table

    Dmnl

    lookup

    The table relating income expectations to family size (FRSNT#41.1).
    """
    return functions.lookup(x, [-0.2, -0.1, 0, 0.1, 0.2], [0.5, 0.6, 0.7, 0.85, 1])


@cache('step')
def fecundity_multiplier():
    """
    fecundity multiplier

    Dmnl

    component

    FECUNDITY MULTIPLIER (FM#34).
    """
    return fecundity_multiplier_table(life_expectancy() / one_year())


def fecundity_multiplier_table(x):
    """
    fecundity multiplier table

    Dmnl

    lookup

    Table relating life expectancy to fecundity (FMT#34.1).
    """
    return functions.lookup(x, [0, 10, 20, 30, 40, 50, 60, 70, 80],
                            [0, 0.2, 0.4, 0.6, 0.7, 0.75, 0.79, 0.84, 0.87])


@cache('step')
def fertility_control_allocation_per_capita():
    """
    fertility control allocation per capita

    $/(Person*year)

    component

    FERTILITY CONTROL ALLOCATIONS PER CAPITA (FCAPC#47).
    """
    return fraction_services_allocated_to_fertility_control() * service_output_per_capita()


@cache('step')
def fertility_control_effectiveness():
    """
    fertility control effectiveness

    Dmnl

    component

    Fertility control effectiveness (FCE#45).
    """
    return functions.if_then_else(time() >= fertility_control_effectiveness_time(), 1,
                                  (fertility_control_effectiveness_table(
                                      fertility_control_facilities_per_capita() / gdp_pc_unit())))


def fertility_control_effectiveness_table(x):
    """
    fertility control effectiveness table

    Dmnl

    lookup

    Fertility control effectiveness table (FCET#45.2).
    """
    return functions.lookup(x, [0, 0.5, 1, 1.5, 2, 2.5, 3], [0.75, 0.85, 0.9, 0.95, 0.98, 0.99, 1])


@cache('step')
def fertility_control_facilities_per_capita():
    """
    fertility control facilities per capita

    $/(Person*year)

    component

    FERTILITY CONTROL FACILITIES PER CAPITA (FCFPC#46).
    """
    return smooth_fertility_control_allocation_per_capita_health_services_impact_delay_fertility_control_allocation_per_capita_3()

# Circular init!
smooth_fertility_control_allocation_per_capita_health_services_impact_delay_fertility_control_allocation_per_capita_3 = functions.Smooth(
    lambda: fertility_control_allocation_per_capita(), lambda: health_services_impact_delay(),
    lambda: fertility_control_allocation_per_capita(), lambda: 3)

@cache('step')
def fraction_services_allocated_to_fertility_control():
    """
    fraction services allocated to fertility control

    Dmnl

    component

    FRACTION OF SERVICES ALLOCATED TO FERTILITY CONTROL (FSAFC#48).
    """
    return fraction_services_allocated_to_fertility_control_table(need_for_fertility_control())


def fraction_services_allocated_to_fertility_control_table(x):
    """
    fraction services allocated to fertility control table

    Dmnl

    lookup

    Table relating the need for fertility control to services allocated. (FSAFCT#48.1).
    """
    return functions.lookup(x, [0, 2, 4, 6, 8, 10], [0, 0.005, 0.015, 0.025, 0.03, 0.035])


@cache('run')
def income_expectation_averaging_time():
    """
    income expectation averaging time

    year

    constant

    Income expectation averaging time (IEAT#43.1)
    """
    return 3


@cache('run')
def lifetime_perception_delay():
    """
    lifetime perception delay

    year

    constant

    Lifetime perception delay (LPD#37.1)
    """
    return 20


@cache('step')
def maximum_total_fertility():
    """
    maximum total fertility

    Dmnl

    component

    MAXIMUM TOTAL FERTILITY (MTF#33).
    """
    return maximum_total_fertility_normal() * fecundity_multiplier()


@cache('run')
def maximum_total_fertility_normal():
    """
    maximum total fertility normal

    Dmnl

    constant

    The normal maximum fertility that would be realized if people had sufficient food and perfect health. (MTFN#33)
    """
    return 12


@cache('step')
def need_for_fertility_control():
    """
    need for fertility control

    Dmnl

    component

    NEED FOR FERTILITY CONTROL (NFC#44).
    """
    return (maximum_total_fertility() / desired_total_fertility()) - 1


@cache('step')
def perceived_life_expectancy():
    """
    perceived life expectancy

    year

    component

    Perceived life expectancy (PLE#37)
    """
    return smooth_life_expectancy_lifetime_perception_delay_life_expectancy_3()

# Circular init!
smooth_life_expectancy_lifetime_perception_delay_life_expectancy_3 = functions.Smooth(
    lambda: life_expectancy(), lambda: lifetime_perception_delay(), lambda: life_expectancy(),
    lambda: 3)
    
@cache('run')
def reproductive_lifetime():
    """
    reproductive lifetime

    year

    constant

    The number of years people can reproduce (RLT#30.1)
    """
    return 30


@cache('step')
def social_family_size_normal():
    """
    social family size normal

    Dmnl

    component

    SOCIAL FAMILY SIZE NORM (SFN#39).
    """
    return social_family_size_normal_table(delayed_industrial_output_per_capita() / gdp_pc_unit())


def social_family_size_normal_table(x):
    """
    social family size normal table

    Dmnl

    lookup

    Table relating material well being to family size (SFNT#39.1)
    """
    return functions.lookup(x, [0, 200, 400, 600, 800], [1.25, 0.94, 0.715, 0.59, 0.5])


@cache('run')
def social_adjustment_delay():
    """
    social adjustment delay

    year

    constant

    SOCIAL ADJUSTMENT DELAY (SAD#40.1).
    """
    return 20


@cache('run')
def fertility_control_effectiveness_time():
    """
    fertility control effectiveness time

    year

    constant

    FERTILITY CONTROL EFFECTIVENESS SET TIME (FCEST#45.1).
    """
    return 4000


@cache('run')
def population_equilibrium_time():
    """
    population equilibrium time

    year

    constant

    The time at which, as a model test, the population is forced to remain constant. (PET#30.2)
    """
    return 4000


@cache('run')
def zero_population_growth_time():
    """
    zero population growth time

    year

    constant

    TIME WHEN DESIRED FAMILY SIZE EQUALS 2 CHILDREN (ZPGT#38.1)
    """
    return 4000


@cache('run')
def thousand():
    """
    THOUSAND

    C

    constant

    Units converted for /1000 rates (--).
    """
    return 1000


@cache('step')
def total_fertility():
    """
    total fertility

    Dmnl

    component

    TOTAL FERTILITY (TF#32).
    """
    return np.minimum(maximum_total_fertility(),
                      (maximum_total_fertility() * (1 - fertility_control_effectiveness()) +
                       desired_total_fertility() * fertility_control_effectiveness()))


