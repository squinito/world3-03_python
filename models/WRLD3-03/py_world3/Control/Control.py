@cache('run')
def final_time():
    """
    FINAL TIME

    year

    constant

    The time at which simulation stops.
    """
    return 2100


@cache('run')
def initial_time():
    """
    INITIAL TIME

    year

    constant

    The time at which the simulation begins.
    """
    return 1900


@cache('step')
def saveper():
    """
    SAVEPER

    year

    component

    The frequency with which results are saved.
    """
    return time_step()


@cache('run')
def policy_year():
    """
    POLICY YEAR

    year

    constant

    The time at which policies are implemented (PYEAR#150.1).
    """
    return 1995


@cache('run')
def time_step():
    """
    TIME STEP

    year

    constant

    The time step for computing the model
    """
    return 0.5


