@cache('step')
def perceived_food_ratio():
    """
    Perceived Food Ratio

    Dmnl

    component

    PERCEIVED FOOD RATIO (PFR#128).
    """
    return smooth_food_ratio_food_shortage_perception_delay_food_ratio_1()


#Circular init!
smooth_food_ratio_food_shortage_perception_delay_food_ratio_1 = functions.Smooth(
    lambda: food_ratio(), lambda: food_shortage_perception_delay(), lambda: 1.0,
    lambda: 1)

@cache('step')
def food_ratio():
    """
    food ratio

    Dmnl

    component

    FOOD RATIO (FR#127)
    """
    return functions.active_initial(food_per_capita() / subsistence_food_per_capita(), 1)


@cache('run')
def food_shortage_perception_delay():
    """
    food shortage perception delay

    year

    constant

    FOOD SHORTAGE PERCEPTION DELAY (FSPD#128.2)
    """
    return 2


@cache('step')
def fraction_of_agricultural_inputs_for_land_maintenance():
    """
    fraction of agricultural inputs for land maintenance

    Dmnl

    component

    FRACTION OF INPUTS ALLOCATED TO LAND MAINTENANCE (FALM#126).
    """
    return fraction_of_agricultural_inputs_for_land_maintenance_table(perceived_food_ratio())


def fraction_of_agricultural_inputs_for_land_maintenance_table(x):
    """
    fraction of agricultural inputs for land maintenance table

    Dmnl

    lookup

    Table relating the perceived food ratio to the fraction of input used for land maintenance (FALMT#126.1).
    """
    return functions.lookup(x, [0, 1, 2, 3, 4], [0, 0.04, 0.07, 0.09, 0.1])


@cache('run')
def subsistence_food_per_capita():
    """
    subsistence food per capita

    Veg eq kg/(Person*year)

    constant

    Subsistence food per capita (SFPC#127.1).
    """
    return 230


