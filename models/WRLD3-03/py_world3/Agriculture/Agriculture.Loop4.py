@cache('step')
def land_fertility_degredation():
    """
    land fertility degredation

    Veg eq kg/(year*year*hectare)

    component

    LAND FERTILITY DEGRADATION (LFD#123).
    """
    return land_fertility() * land_fertility_degredation_rate()


@cache('step')
def land_fertility_degredation_rate():
    """
    land fertility degredation rate

    1/year

    component

    Land fertility degradation rate (LFDR#122).
    """
    return land_fertility_degredation_rate_table(persistent_pollution_index())


def land_fertility_degredation_rate_table(x):
    """
    land fertility degredation rate table

    1/year

    lookup

    Table relating persistent pollution to land fertility degradation (LFDRT#122.1).
    """
    return functions.lookup(x, [0, 10, 20, 30], [0, 0.1, 0.3, 0.5])


@cache('step')
def land_fertility():
    """
    Land Fertility

    Veg eq kg/(year*hectare)

    component

    Land fertility (LFERT#121).
    """
    return integ_land_fertility()


integ_land_fertility = functions.Integ(
    lambda: (land_fertility_regeneration() - land_fertility_degredation()),
    lambda: initial_land_fertility())

@cache('run')
def initial_land_fertility():
    """
    initial land fertility

    Veg eq kg/(year*hectare)

    constant

    LAND FERTILITY INITIAL (LFERTI#121.2)
    """
    return 600


