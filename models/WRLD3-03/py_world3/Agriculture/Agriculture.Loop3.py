@cache('step')
def average_life_of_land():
    """
    average life of land

    year

    component

    Average life of land (ALL#112).
    """
    return average_life_of_land_normal() * land_life_multiplier_from_land_yield()


@cache('run')
def average_life_of_land_normal():
    """
    average life of land normal

    year

    constant

    AVERAGE LIFE OF LAND NORMAL (ALLN#112.1).
    """
    return 1000


@cache('step')
def land_erosion_rate():
    """
    land erosion rate

    hectare/year

    component

    Land erosion rate (LER#
    """
    return arable_land() / average_life_of_land()


@cache('step')
def land_removal_for_urban_and_industrial_use():
    """
    land removal for urban and industrial use

    hectare/year

    component

    LAND REMOVAL FOR URBAN-INDUSTRIAL USE (LRUI#119).
    """
    return np.maximum(0,
                      urban_and_industrial_land_required() -
                      urban_and_industrial_land()) / urban_and_industrial_land_development_time()


@cache('step')
def land_life_multiplier_from_land_yield_1():
    """
    land life multiplier from land yield 1

    Dmnl

    component

    Land life multiplier from yield before switch time (LLMY1#114).
    """
    return land_life_multiplier_from_land_yield_table_1(land_yield() / inherent_land_fertility())


def land_life_multiplier_from_land_yield_table_1(x):
    """
    land life multiplier from land yield table 1

    Dmnl

    lookup

    Table relating yield to the effect on land life (LLMY1T#114.1).
    """
    return functions.lookup(x, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                            [1.2, 1, 0.63, 0.36, 0.16, 0.055, 0.04, 0.025, 0.015, 0.01])


@cache('step')
def land_life_multiplier_from_land_yield_2():
    """
    land life multiplier from land yield 2

    Dmnl

    component

    Land life multiplier from yield after switch time (LLMY2#115).
    """
    return land_life_multiplier_from_land_yield_table_2(land_yield() / inherent_land_fertility())


def land_life_multiplier_from_land_yield_table_2(x):
    """
    land life multiplier from land yield table 2

    Dmnl

    lookup

    Table relating yield to the effect on land life (LLMY2T#115.1).
    """
    return functions.lookup(x, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                            [1.2, 1, 0.63, 0.36, 0.29, 0.26, 0.24, 0.22, 0.21, 0.2])


@cache('step')
def land_life_multiplier_from_land_yield():
    """
    land life multiplier from land yield

    Dmnl

    component

    LAND LIFE MULTIPLIER FROM YIELD (LLMY#113).
    """
    return functions.if_then_else(
        time() >= land_life_policy_implementation_time(),
        (0.95**((time() - land_life_policy_implementation_time()) / one_year())) *
        land_life_multiplier_from_land_yield_1() + (1 - 0.95**(
            (time() - land_life_policy_implementation_time()) / one_year())) *
        land_life_multiplier_from_land_yield_2(), land_life_multiplier_from_land_yield_1())


@cache('run')
def land_life_policy_implementation_time():
    """
    land life policy implementation time

    year

    constant

    Land life multiplier from yield switch time (LLMYTM#--)
    """
    return 4000


@cache('run')
def urban_and_industrial_land_development_time():
    """
    urban and industrial land development time

    year

    constant

    Urban industrial land development time (UILDT#119.1).
    """
    return 10


@cache('step')
def urban_and_industrial_land_required_per_capita():
    """
    urban and industrial land required per capita

    hectare/Person

    component

    Urban industrial land per capita (UILPC#117).
    """
    return urban_and_industrial_land_required_per_capita_table(
        industrial_output_per_capita() / gdp_pc_unit())


def urban_and_industrial_land_required_per_capita_table(x):
    """
    urban and industrial land required per capita table

    hectare/Person

    lookup

    Table relating industrial output to urban industrial land (UILPCT#117.1)
    """
    return functions.lookup(x, [0, 200, 400, 600, 800, 1000, 1200, 1400, 1600],
                            [0.005, 0.008, 0.015, 0.025, 0.04, 0.055, 0.07, 0.08, 0.09])


@cache('step')
def urban_and_industrial_land_required():
    """
    urban and industrial land required

    hectare

    component

    Urban industrial land required (UILR#118).
    """
    return urban_and_industrial_land_required_per_capita() * population()


@cache('step')
def urban_and_industrial_land():
    """
    Urban and Industrial Land

    hectare

    component

    URBAN-INDUSTRIAL LAND (UIL#120).
    """
    return integ_urban_and_industrial_land()


integ_urban_and_industrial_land = functions.Integ(
    lambda: (land_removal_for_urban_and_industrial_use()),
    lambda: initial_urban_and_industrial_land())
    
@cache('run')
def initial_urban_and_industrial_land():
    """
    initial urban and industrial land

    hectare

    constant

    URBAN-INDUSTRIAL LAND INITIAL (UILI#120.1).
    """
    return 8.2e+06


