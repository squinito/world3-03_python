@cache('run')
def gdp_pc_unit():
    """
    GDP pc unit

    $/Person/year

    constant


    """
    return 1


@cache('run')
def unit_agricultural_input():
    """
    unit agricultural input

    $/hectare/year

    constant


    """
    return 1


@cache('run')
def unit_population():
    """
    unit population

    Person

    constant


    """
    return 1


@cache('step')
def absorption_land_gha():
    """
    "Absorption Land (GHA)"

    Ghectares

    component


    """
    return persistent_pollution_generation_rate() * ha_per_unit_of_pollution() / ha_per_gha()


@cache('step')
def arable_land_in_gigahectares_gha():
    """
    "Arable Land in Gigahectares (GHA)"

    Ghectares

    component


    """
    return arable_land() / ha_per_gha()


@cache('step')
def education_index():
    """
    Education Index

    Dmnl

    component


    """
    return education_index_lookup(gdp_per_capita() / gdp_pc_unit())


def education_index_lookup(x):
    """
    Education Index LOOKUP

    Dmnl

    lookup


    """
    return functions.lookup(x, [0, 1000, 2000, 3000, 4000, 5000, 6000, 7000],
                            [0, 0.81, 0.88, 0.92, 0.95, 0.98, 0.99, 1])


@cache('step')
def gdp_index():
    """
    GDP Index

    Dmnl

    component


    """
    return functions.log(gdp_per_capita() / ref_lo_gdp(), 10) / functions.log(
        ref_hi_gdp() / ref_lo_gdp(), 10)


@cache('step')
def gdp_per_capita():
    """
    GDP per capita

    $/(year*Person)

    component


    """
    return gdp_per_capita_lookup(industrial_output_per_capita() / gdp_pc_unit())


def gdp_per_capita_lookup(x):
    """
    GDP per capita LOOKUP

    $/(year*Person)

    lookup


    """
    return functions.lookup(x, [0, 200, 400, 600, 800, 1000], [120, 600, 1200, 1800, 2500, 3200])


@cache('run')
def ha_per_gha():
    """
    ha per Gha

    hectare/Ghectare

    constant


    """
    return 1e+09


@cache('run')
def ha_per_unit_of_pollution():
    """
    ha per unit of pollution

    hectares/(Pollution units/year)

    constant


    """
    return 4


@cache('step')
def human_ecological_footprint():
    """
    Human Ecological Footprint

    Dmnl

    component

    See Appendix 2 of Limits to Growth - the 30-Year Update for discussion of 
        this index
    """
    return (arable_land_in_gigahectares_gha() + urban_land_gha() +
            absorption_land_gha()) / total_land()


@cache('step')
def human_welfare_index():
    """
    Human Welfare Index

    Dmnl

    component

    See Appendix 2 of Limits to Growth - the 30-Year Update for discussion of 
        this index
    """
    return (life_expectancy_index() + education_index() + gdp_index()) / 3


@cache('step')
def life_expectancy_index():
    """
    Life Expectancy Index

    Dmnl

    component


    """
    return life_expectancy_index_lookup(life_expectancy() / one_year())


def life_expectancy_index_lookup(x):
    """
    Life Expectancy Index LOOKUP

    Dmnl

    lookup


    """
    return functions.lookup(x, [25, 35, 45, 55, 65, 75, 85], [0, 0.16, 0.33, 0.5, 0.67, 0.84, 1])


@cache('run')
def one_year():
    """
    one year

    year

    constant


    """
    return 1


@cache('run')
def ref_hi_gdp():
    """
    Ref Hi GDP

    $/(year*Person)

    constant


    """
    return 9508


@cache('run')
def ref_lo_gdp():
    """
    Ref Lo GDP

    $/(year*Person)

    constant


    """
    return 24


@cache('run')
def total_land():
    """
    Total Land

    Ghectares

    constant


    """
    return 1.91


@cache('step')
def urban_land_gha():
    """
    "Urban Land (GHA)"

    Ghectares

    component


    """
    return urban_and_industrial_land() / ha_per_gha()


