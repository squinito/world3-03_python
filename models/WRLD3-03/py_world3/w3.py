"""
Python model "wrld3-03+.py"
Translated using PySD version 0.8.3
"""
from __future__ import division
import numpy as np
from pysd import utils
import xarray as xr

from pysd.py_backend.functions import cache
from pysd.py_backend import functions

_subscript_dict = {}

_namespace = {
    'TIME':
    'time',
    'Time':
    'time',
    'GDP pc unit':
    'gdp_pc_unit',
    'unit agricultural input':
    'unit_agricultural_input',
    'unit population':
    'unit_population',
    '"Absorption Land (GHA)"':
    'absorption_land_gha',
    '"Arable Land in Gigahectares (GHA)"':
    'arable_land_in_gigahectares_gha',
    'Education Index':
    'education_index',
    'Education Index LOOKUP':
    'education_index_lookup',
    'GDP Index':
    'gdp_index',
    'GDP per capita':
    'gdp_per_capita',
    'GDP per capita LOOKUP':
    'gdp_per_capita_lookup',
    'ha per Gha':
    'ha_per_gha',
    'ha per unit of pollution':
    'ha_per_unit_of_pollution',
    'Human Ecological Footprint':
    'human_ecological_footprint',
    'Human Welfare Index':
    'human_welfare_index',
    'Life Expectancy Index':
    'life_expectancy_index',
    'Life Expectancy Index LOOKUP':
    'life_expectancy_index_lookup',
    'one year':
    'one_year',
    'Ref Hi GDP':
    'ref_hi_gdp',
    'Ref Lo GDP':
    'ref_lo_gdp',
    'Total Land':
    'total_land',
    '"Urban Land (GHA)"':
    'urban_land_gha',
    'Arable Land':
    'arable_land',
    'initial arable land':
    'initial_arable_land',
    'development cost per hectare':
    'development_cost_per_hectare',
    'development cost per hectare table':
    'development_cost_per_hectare_table',
    'food':
    'food',
    'food per capita':
    'food_per_capita',
    'land development rate':
    'land_development_rate',
    'land fr cult':
    'land_fr_cult',
    'land fraction harvested':
    'land_fraction_harvested',
    'fraction of industrial output allocated to agriculture 1':
    'fraction_of_industrial_output_allocated_to_agriculture_1',
    'fraction industrial output allocated to agriculture table 1':
    'fraction_industrial_output_allocated_to_agriculture_table_1',
    'fraction of industrial output allocated to agriculture 2':
    'fraction_of_industrial_output_allocated_to_agriculture_2',
    'fraction industrial output allocated to agriculture table 2':
    'fraction_industrial_output_allocated_to_agriculture_table_2',
    'indicated food per capita 1':
    'indicated_food_per_capita_1',
    'indicated food per capita table 1':
    'indicated_food_per_capita_table_1',
    'indicated food per capita 2':
    'indicated_food_per_capita_2',
    'indicated food per capita table 2':
    'indicated_food_per_capita_table_2',
    'Potentially Arable Land':
    'potentially_arable_land',
    'initial potentially arable land':
    'initial_potentially_arable_land',
    'potentially arable land total':
    'potentially_arable_land_total',
    'processing loss':
    'processing_loss',
    'fraction of industrial output allocated to agriculture':
    'fraction_of_industrial_output_allocated_to_agriculture',
    'indicated food per capita':
    'indicated_food_per_capita',
    'total agricultural investment':
    'total_agricultural_investment',
    'Agricultural Inputs':
    'agricultural_inputs',
    'average life agricultural inputs':
    'average_life_agricultural_inputs',
    'agricultural input per hectare':
    'agricultural_input_per_hectare',
    'current agricultural inputs':
    'current_agricultural_inputs',
    'desired food ratio':
    'desired_food_ratio',
    'IND OUT IN 1970':
    'ind_out_in_1970',
    'land yield':
    'land_yield',
    'land yield multiplier from capital':
    'land_yield_multiplier_from_capital',
    'land yield multiplier from capital table':
    'land_yield_multiplier_from_capital_table',
    'average life of agricultural inputs 1':
    'average_life_of_agricultural_inputs_1',
    'average life of agricultural inputs 2':
    'average_life_of_agricultural_inputs_2',
    'land yield factor 1':
    'land_yield_factor_1',
    'land yield factor 2':
    'land_yield_factor_2',
    'land yield multipler from air pollution 1':
    'land_yield_multipler_from_air_pollution_1',
    'land yield multipler from air pollution table 1':
    'land_yield_multipler_from_air_pollution_table_1',
    'land yield multiplier from air pollution 2':
    'land_yield_multiplier_from_air_pollution_2',
    'land yield multipler from air pollution table 2':
    'land_yield_multipler_from_air_pollution_table_2',
    'land yield technology change rate multiplier':
    'land_yield_technology_change_rate_multiplier',
    'land yield technology change rate multiplier table':
    'land_yield_technology_change_rate_multiplier_table',
    'land yield multiplier from technology':
    'land_yield_multiplier_from_technology',
    'land yield multiplier from air pollution':
    'land_yield_multiplier_from_air_pollution',
    'air pollution policy implementation time':
    'air_pollution_policy_implementation_time',
    'Land Yield Technology':
    'land_yield_technology',
    'land yield technology change rate':
    'land_yield_technology_change_rate',
    'average life of land':
    'average_life_of_land',
    'average life of land normal':
    'average_life_of_land_normal',
    'land erosion rate':
    'land_erosion_rate',
    'land removal for urban and industrial use':
    'land_removal_for_urban_and_industrial_use',
    'land life multiplier from land yield 1':
    'land_life_multiplier_from_land_yield_1',
    'land life multiplier from land yield table 1':
    'land_life_multiplier_from_land_yield_table_1',
    'land life multiplier from land yield 2':
    'land_life_multiplier_from_land_yield_2',
    'land life multiplier from land yield table 2':
    'land_life_multiplier_from_land_yield_table_2',
    'land life multiplier from land yield':
    'land_life_multiplier_from_land_yield',
    'land life policy implementation time':
    'land_life_policy_implementation_time',
    'urban and industrial land development time':
    'urban_and_industrial_land_development_time',
    'urban and industrial land required per capita':
    'urban_and_industrial_land_required_per_capita',
    'urban and industrial land required per capita table':
    'urban_and_industrial_land_required_per_capita_table',
    'urban and industrial land required':
    'urban_and_industrial_land_required',
    'Urban and Industrial Land':
    'urban_and_industrial_land',
    'initial urban and industrial land':
    'initial_urban_and_industrial_land',
    'land fertility degredation':
    'land_fertility_degredation',
    'land fertility degredation rate':
    'land_fertility_degredation_rate',
    'land fertility degredation rate table':
    'land_fertility_degredation_rate_table',
    'Land Fertility':
    'land_fertility',
    'initial land fertility':
    'initial_land_fertility',
    'inherent land fertility':
    'inherent_land_fertility',
    'land fertility regeneration':
    'land_fertility_regeneration',
    'land fertility regeneration time':
    'land_fertility_regeneration_time',
    'land fertility regeneration time table':
    'land_fertility_regeneration_time_table',
    'Perceived Food Ratio':
    'perceived_food_ratio',
    'food ratio':
    'food_ratio',
    'food shortage perception delay':
    'food_shortage_perception_delay',
    'fraction of agricultural inputs for land maintenance':
    'fraction_of_agricultural_inputs_for_land_maintenance',
    'fraction of agricultural inputs for land maintenance table':
    'fraction_of_agricultural_inputs_for_land_maintenance_table',
    'subsistence food per capita':
    'subsistence_food_per_capita',
    'fraction of agricultural inputs allocated to land development':
    'fraction_of_agricultural_inputs_allocated_to_land_development',
    'fraction of agricultural inputs allocated to land development table':
    'fraction_of_agricultural_inputs_allocated_to_land_development_table',
    'marginal land yield multiplier from capital':
    'marginal_land_yield_multiplier_from_capital',
    'marginal land yield multiplier from capital table':
    'marginal_land_yield_multiplier_from_capital_table',
    'marginal productivity of agricultural inputs':
    'marginal_productivity_of_agricultural_inputs',
    'marginal productivity of land development':
    'marginal_productivity_of_land_development',
    'social discount':
    'social_discount',
    'industrial capital output ratio multiplier from resource conservation technology':
    'industrial_capital_output_ratio_multiplier_from_resource_conservation_technology',
    'industrial capital output ratio multiplier from pollution technology':
    'industrial_capital_output_ratio_multiplier_from_pollution_technology',
    'industrial capital output ratio multiplier from land yield technology':
    'industrial_capital_output_ratio_multiplier_from_land_yield_technology',
    'fraction of industrial output allocated to investment':
    'fraction_of_industrial_output_allocated_to_investment',
    'industrial capital depreciation':
    'industrial_capital_depreciation',
    'industrial capital investment':
    'industrial_capital_investment',
    'industrial capital output ratio multiplier from resource table':
    'industrial_capital_output_ratio_multiplier_from_resource_table',
    'industrial output per capita':
    'industrial_output_per_capita',
    'industrial output per capita desired':
    'industrial_output_per_capita_desired',
    'Industrial Capital':
    'industrial_capital',
    'initial industrial capital':
    'initial_industrial_capital',
    'industrial output':
    'industrial_output',
    'average life of industrial capital 1':
    'average_life_of_industrial_capital_1',
    'average life of industrial capital 2':
    'average_life_of_industrial_capital_2',
    'fraction of industrial output allocated to consumption constant':
    'fraction_of_industrial_output_allocated_to_consumption_constant',
    'fraction of industrial output allocated to consumption constant 1':
    'fraction_of_industrial_output_allocated_to_consumption_constant_1',
    'fraction of industrial output allocated to consumption constant 2':
    'fraction_of_industrial_output_allocated_to_consumption_constant_2',
    'fraction of industrial output allocated to consumption variable':
    'fraction_of_industrial_output_allocated_to_consumption_variable',
    'fraction of industrial output allocated to consumption variable table':
    'fraction_of_industrial_output_allocated_to_consumption_variable_table',
    'industrial capital output ratio 1':
    'industrial_capital_output_ratio_1',
    'industrial capital output ratio 2':
    'industrial_capital_output_ratio_2',
    'industrial capital output ratio multiplier from pollution table':
    'industrial_capital_output_ratio_multiplier_from_pollution_table',
    'average life of industrial capital':
    'average_life_of_industrial_capital',
    'fraction of industrial output allocated to consumption':
    'fraction_of_industrial_output_allocated_to_consumption',
    'industrial capital output ratio':
    'industrial_capital_output_ratio',
    'industrial equilibrium time':
    'industrial_equilibrium_time',
    'industrial capital output ratio multiplier table':
    'industrial_capital_output_ratio_multiplier_table',
    'Delayed Labor Utilization Fraction':
    'delayed_labor_utilization_fraction',
    'capacity utilization fraction':
    'capacity_utilization_fraction',
    'capacity utilization fraction table':
    'capacity_utilization_fraction_table',
    'jobs':
    'jobs',
    'jobs per hectare':
    'jobs_per_hectare',
    'jobs per hectare table':
    'jobs_per_hectare_table',
    'jobs per industrial capital unit':
    'jobs_per_industrial_capital_unit',
    'jobs per industrial capital unit table':
    'jobs_per_industrial_capital_unit_table',
    'jobs per service capital unit':
    'jobs_per_service_capital_unit',
    'jobs per service capital unit table':
    'jobs_per_service_capital_unit_table',
    'labor force':
    'labor_force',
    'labor force participation fraction':
    'labor_force_participation_fraction',
    'labor utilization fraction':
    'labor_utilization_fraction',
    'labor utilization fraction delay time':
    'labor_utilization_fraction_delay_time',
    'potential jobs agricultural sector':
    'potential_jobs_agricultural_sector',
    'potential jobs industrial sector':
    'potential_jobs_industrial_sector',
    'potential jobs service sector':
    'potential_jobs_service_sector',
    'average life of service capital 1':
    'average_life_of_service_capital_1',
    'average life of service capital 2':
    'average_life_of_service_capital_2',
    'fraction of industrial output allocated to services 1':
    'fraction_of_industrial_output_allocated_to_services_1',
    'fraction of industrial output allocated to services table 1':
    'fraction_of_industrial_output_allocated_to_services_table_1',
    'fraction of industrial output allocated to services 2':
    'fraction_of_industrial_output_allocated_to_services_2',
    'fraction of industrial output allocated to services table 2':
    'fraction_of_industrial_output_allocated_to_services_table_2',
    'indicated services output per capita 1':
    'indicated_services_output_per_capita_1',
    'indicated services output per capita table 1':
    'indicated_services_output_per_capita_table_1',
    'indicated services output per capita 2':
    'indicated_services_output_per_capita_2',
    'indicated services output per capita table 2':
    'indicated_services_output_per_capita_table_2',
    'service capital output ratio 1':
    'service_capital_output_ratio_1',
    'service capital output ratio 2':
    'service_capital_output_ratio_2',
    'average life of service capital':
    'average_life_of_service_capital',
    'fraction of industrial output allocated to services':
    'fraction_of_industrial_output_allocated_to_services',
    'indicated services output per capita':
    'indicated_services_output_per_capita',
    'service capital output ratio':
    'service_capital_output_ratio',
    'service capital depreciation':
    'service_capital_depreciation',
    'service capital investment':
    'service_capital_investment',
    'service output per capita':
    'service_output_per_capita',
    'Service Capital':
    'service_capital',
    'initial service capital':
    'initial_service_capital',
    'service output':
    'service_output',
    'FINAL TIME':
    'final_time',
    'INITIAL TIME':
    'initial_time',
    'SAVEPER':
    'saveper',
    'POLICY YEAR':
    'policy_year',
    'TIME STEP':
    'time_step',
    'agricultural material toxicity index':
    'agricultural_material_toxicity_index',
    'assimilation half life':
    'assimilation_half_life',
    'assimilation half life in 1970':
    'assimilation_half_life_in_1970',
    'assimilation half life multiplier':
    'assimilation_half_life_multiplier',
    'assimilation half life mult table':
    'assimilation_half_life_mult_table',
    'desired persistent pollution index':
    'desired_persistent_pollution_index',
    'fraction of agricultural inputs from persistent materials':
    'fraction_of_agricultural_inputs_from_persistent_materials',
    'fraction of resources from persistent materials':
    'fraction_of_resources_from_persistent_materials',
    'industrial material toxicity index':
    'industrial_material_toxicity_index',
    'industrial material emissions factor':
    'industrial_material_emissions_factor',
    'persistent pollution generation factor 1':
    'persistent_pollution_generation_factor_1',
    'persistent pollution generation factor 2':
    'persistent_pollution_generation_factor_2',
    'persistent pollution technology change multiplier':
    'persistent_pollution_technology_change_multiplier',
    'persistent pollution technology change mult table':
    'persistent_pollution_technology_change_mult_table',
    'Persistent Pollution':
    'persistent_pollution',
    'initial persistent pollution':
    'initial_persistent_pollution',
    'persistent pollution generation industry':
    'persistent_pollution_generation_industry',
    'persistent pollution generation agriculture':
    'persistent_pollution_generation_agriculture',
    'persistent pollution generation rate':
    'persistent_pollution_generation_rate',
    'persistent pollution appearance rate':
    'persistent_pollution_appearance_rate',
    'persistent pollution assimilation rate':
    'persistent_pollution_assimilation_rate',
    'persistent pollution in 1970':
    'persistent_pollution_in_1970',
    'persistent pollution index':
    'persistent_pollution_index',
    'Persistent Pollution Technology':
    'persistent_pollution_technology',
    'persistent pollution technology change rate':
    'persistent_pollution_technology_change_rate',
    'persistent pollution transmission delay':
    'persistent_pollution_transmission_delay',
    'persistent pollution generation factor':
    'persistent_pollution_generation_factor',
    'deaths 0 to 14':
    'deaths_0_to_14',
    'deaths 15 to 44':
    'deaths_15_to_44',
    'deaths 45 to 64':
    'deaths_45_to_64',
    'deaths 65 plus':
    'deaths_65_plus',
    'maturation 14 to 15':
    'maturation_14_to_15',
    'maturation 44 to 45':
    'maturation_44_to_45',
    'maturation 64 to 65':
    'maturation_64_to_65',
    'mortality 45 to 64':
    'mortality_45_to_64',
    'mortality 45 to 64 table':
    'mortality_45_to_64_table',
    'mortality 65 plus':
    'mortality_65_plus',
    'mortality 65 plus table':
    'mortality_65_plus_table',
    'mortality 0 to 14':
    'mortality_0_to_14',
    'mortality 0 to 14 table':
    'mortality_0_to_14_table',
    'mortality 15 to 44':
    'mortality_15_to_44',
    'mortality 15 to 44 table':
    'mortality_15_to_44_table',
    'Population 0 To 14':
    'population_0_to_14',
    'initial population 0 to 14':
    'initial_population_0_to_14',
    'Population 15 To 44':
    'population_15_to_44',
    'initial population 15 to 44':
    'initial_population_15_to_44',
    'Population 45 To 64':
    'population_45_to_64',
    'initial population 54 to 64':
    'initial_population_54_to_64',
    'Population 65 Plus':
    'population_65_plus',
    'initial population 65 plus':
    'initial_population_65_plus',
    'population':
    'population',
    'average industrial output per capita':
    'average_industrial_output_per_capita',
    'birth rate':
    'birth_rate',
    'births':
    'births',
    'completed multiplier from perceived lifetime':
    'completed_multiplier_from_perceived_lifetime',
    'completed multiplier from perceived lifetime table':
    'completed_multiplier_from_perceived_lifetime_table',
    'delayed industrial output per capita':
    'delayed_industrial_output_per_capita',
    'desired completed family size':
    'desired_completed_family_size',
    'desired completed family size normal':
    'desired_completed_family_size_normal',
    'desired total fertility':
    'desired_total_fertility',
    'family income expectation':
    'family_income_expectation',
    'family response to social norm':
    'family_response_to_social_norm',
    'family response to social norm table':
    'family_response_to_social_norm_table',
    'fecundity multiplier':
    'fecundity_multiplier',
    'fecundity multiplier table':
    'fecundity_multiplier_table',
    'fertility control allocation per capita':
    'fertility_control_allocation_per_capita',
    'fertility control effectiveness':
    'fertility_control_effectiveness',
    'fertility control effectiveness table':
    'fertility_control_effectiveness_table',
    'fertility control facilities per capita':
    'fertility_control_facilities_per_capita',
    'fraction services allocated to fertility control':
    'fraction_services_allocated_to_fertility_control',
    'fraction services allocated to fertility control table':
    'fraction_services_allocated_to_fertility_control_table',
    'income expectation averaging time':
    'income_expectation_averaging_time',
    'lifetime perception delay':
    'lifetime_perception_delay',
    'maximum total fertility':
    'maximum_total_fertility',
    'maximum total fertility normal':
    'maximum_total_fertility_normal',
    'need for fertility control':
    'need_for_fertility_control',
    'perceived life expectancy':
    'perceived_life_expectancy',
    'reproductive lifetime':
    'reproductive_lifetime',
    'social family size normal':
    'social_family_size_normal',
    'social family size normal table':
    'social_family_size_normal_table',
    'social adjustment delay':
    'social_adjustment_delay',
    'fertility control effectiveness time':
    'fertility_control_effectiveness_time',
    'population equilibrium time':
    'population_equilibrium_time',
    'zero population growth time':
    'zero_population_growth_time',
    'THOUSAND':
    'thousand',
    'total fertility':
    'total_fertility',
    'crowding multiplier from industry':
    'crowding_multiplier_from_industry',
    'crowding multiplier from industry table':
    'crowding_multiplier_from_industry_table',
    'death rate':
    'death_rate',
    'deaths':
    'deaths',
    'effective health services per capita':
    'effective_health_services_per_capita',
    'fraction of population urban':
    'fraction_of_population_urban',
    'fraction of population urban table':
    'fraction_of_population_urban_table',
    'health services per capita':
    'health_services_per_capita',
    'health services per capita table':
    'health_services_per_capita_table',
    'health services impact delay':
    'health_services_impact_delay',
    'life expectancy normal':
    'life_expectancy_normal',
    'life expectancy':
    'life_expectancy',
    'lifetime multiplier from crowding':
    'lifetime_multiplier_from_crowding',
    'lifetime multiplier from food':
    'lifetime_multiplier_from_food',
    'lifetime multiplier from food table':
    'lifetime_multiplier_from_food_table',
    'lifetime multiplier from health services':
    'lifetime_multiplier_from_health_services',
    'lifetime multiplier from health services 1':
    'lifetime_multiplier_from_health_services_1',
    'lifetime multiplier from health services 1 table':
    'lifetime_multiplier_from_health_services_1_table',
    'lifetime multiplier from health services 2':
    'lifetime_multiplier_from_health_services_2',
    'lifetime multiplier from health services 2 table':
    'lifetime_multiplier_from_health_services_2_table',
    'lifetime multiplier from persistent pollution':
    'lifetime_multiplier_from_persistent_pollution',
    'lifetime multiplier from persistent pollution table':
    'lifetime_multiplier_from_persistent_pollution_table',
    'desired resource use rate':
    'desired_resource_use_rate',
    'fraction of resources remaining':
    'fraction_of_resources_remaining',
    'resource usage rate':
    'resource_usage_rate',
    'initial nonrenewable resources':
    'initial_nonrenewable_resources',
    'Nonrenewable Resources':
    'nonrenewable_resources',
    'fraction of capital allocated to obtaining resources 1':
    'fraction_of_capital_allocated_to_obtaining_resources_1',
    'fraction of capital allocated to obtaining resources 1 table':
    'fraction_of_capital_allocated_to_obtaining_resources_1_table',
    'fraction of capital allocated to obtaining resources 2':
    'fraction_of_capital_allocated_to_obtaining_resources_2',
    'fraction of capital allocated to obtaining resources 2 table':
    'fraction_of_capital_allocated_to_obtaining_resources_2_table',
    'resource use factor 1':
    'resource_use_factor_1',
    'resource use fact 2':
    'resource_use_fact_2',
    'resource technology change rate multiplier':
    'resource_technology_change_rate_multiplier',
    'resource technology change mult table':
    'resource_technology_change_mult_table',
    'per capita resource use multiplier':
    'per_capita_resource_use_multiplier',
    'per capita resource use mult table':
    'per_capita_resource_use_mult_table',
    'Resource Conservation Technology':
    'resource_conservation_technology',
    'resource technology change rate':
    'resource_technology_change_rate',
    'fraction of industrial capital allocated to obtaining resources':
    'fraction_of_industrial_capital_allocated_to_obtaining_resources',
    'resource use factor':
    'resource_use_factor',
    'fraction of industrial capital allocated to obtaining resources switch time':
    'fraction_of_industrial_capital_allocated_to_obtaining_resources_switch_time',
    'technology development delay':
    'technology_development_delay',
    'consumed industrial output':
    'consumed_industrial_output',
    'consumed industrial output per capita':
    'consumed_industrial_output_per_capita',
    'fraction of output in agriculture':
    'fraction_of_output_in_agriculture',
    'fraction of output in industry':
    'fraction_of_output_in_industry',
    'fraction of output in services':
    'fraction_of_output_in_services',
    'persistent pollution intensity industry':
    'persistent_pollution_intensity_industry',
    'PRICE OF FOOD':
    'price_of_food',
    'resource use intensity':
    'resource_use_intensity'
}

__pysd_version__ = "0.8.3"


import os
dir_path = os.path.dirname(os.path.abspath(__file__))
exec(compile(open(dir_path+"/wrld303/wrld303.py").read(),filename="wrld303.py", mode="exec"))
exec(compile(open(dir_path+"/Agriculture/Agriculture.Loop1.py").read(),filename="Agriculture.Loop1.py", mode="exec"))
exec(compile(open(dir_path+"/Agriculture/Agriculture.Loop2.py").read(),filename="Agriculture.Loop2.py", mode="exec"))
exec(compile(open(dir_path+"/Agriculture/Agriculture.Loop3.py").read(),filename="Agriculture.Loop3.py", mode="exec"))
exec(compile(open(dir_path+"/Agriculture/Agriculture.Loop4.py").read(),filename="Agriculture.Loop4.py", mode="exec"))
exec(compile(open(dir_path+"/Agriculture/Agriculture.Loop5.py").read(),filename="Agriculture.Loop5.py", mode="exec"))
exec(compile(open(dir_path+"/Agriculture/Agriculture.Loop6.py").read(),filename="Agriculture.Loop6.py", mode="exec"))
exec(compile(open(dir_path+"/Agriculture/Agriculture.Loops1&2.py").read(),filename="Agriculture.Loops1&2.py", mode="exec"))
exec(compile(open(dir_path+"/Capital/Capital.Industry.py").read(),filename="Capital.Industry.py", mode="exec"))
exec(compile(open(dir_path+"/Capital/Capital.Jobs.py").read(),filename="Capital.Jobs.py", mode="exec"))
exec(compile(open(dir_path+"/Capital/Capital.Service.py").read(),filename="Capital.Service.py", mode="exec"))
exec(compile(open(dir_path+"/Control/Control.py").read(),filename="Control.py", mode="exec"))
exec(compile(open(dir_path+"/Pollution/Pollution.py").read(),filename="Pollution.py", mode="exec"))
exec(compile(open(dir_path+"/Population/Population.py").read(),filename="Population.py", mode="exec"))
exec(compile(open(dir_path+"/Population/Population.Births.py").read(),filename="Population.Births.py", mode="exec"))
exec(compile(open(dir_path+"/Population/Population.Death.py").read(),filename="Population.Death.py", mode="exec"))
exec(compile(open(dir_path+"/Resource/Resource.py").read(),filename="Resource.py", mode="exec"))
exec(compile(open(dir_path+"/Supplementary/Supplementary.py").read(),filename="Supplementary.py", mode="exec"))
