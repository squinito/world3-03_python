@cache('step')
def consumed_industrial_output():
    """
    consumed industrial output

    $/year

    component

    Consumed industrial output (CIO#--).
    """
    return industrial_output() * fraction_of_industrial_output_allocated_to_consumption()


@cache('step')
def consumed_industrial_output_per_capita():
    """
    consumed industrial output per capita

    $/(Person*year)

    component

    Consumption Industrial Output per Capita (CIOPC#--)
    """
    return consumed_industrial_output() / population()


@cache('step')
def fraction_of_output_in_agriculture():
    """
    fraction of output in agriculture

    Dmnl

    component

    FRACTION OF OUTPUT IN AGRICULTURE (FAO#147)
    """
    return (price_of_food() * food()) / (
        price_of_food() * food() + service_output() + industrial_output())


@cache('step')
def fraction_of_output_in_industry():
    """
    fraction of output in industry

    Dmnl

    component

    Fraction of output that is industrial output (FOI#148).
    """
    return industrial_output() / (
        price_of_food() * food() + service_output() + industrial_output())


@cache('step')
def fraction_of_output_in_services():
    """
    fraction of output in services

    Dmnl

    component

    FRACTION OF OUTPUT IN SERVICES (FOS#149).
    """
    return service_output() / (price_of_food() * food() + service_output() + industrial_output())


@cache('step')
def persistent_pollution_intensity_industry():
    """
    persistent pollution intensity industry

    Pollution units/$

    component

    pollution intensity indicator (PLINID#--).
    """
    return persistent_pollution_generation_industry() * persistent_pollution_generation_factor(
    ) / industrial_output()


@cache('run')
def price_of_food():
    """
    PRICE OF FOOD

    $/Veg eq kg

    constant

    The price of food used as a basis for comparing agricultural and industrial output. (--).
    """
    return 0.22


@cache('step')
def resource_use_intensity():
    """
    resource use intensity

    Resource units/$

    component

    ADAPTIVE TECHNOLOGICAL CONTROL CARDS nonrenewable resource usage intensity (RESINT#--)
    """
    return resource_usage_rate() / industrial_output()





