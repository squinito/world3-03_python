{UTF-8}
:MACRO: P EXP(input)
P EXP = EXP(MAX(-700,MIN(700,input)))
	~	input
	~	Exponentiation protected against over/underflow
	|

:END OF MACRO:
:MACRO: SSHAPE(input)
SSHAPE = EXP(MAX(-700,MIN(700,input)))/(1+EXP(MAX(-700,MIN(700,input))))
	~	dmnl
	~	Exponential s-shaped curve; -infinity -> 0, 0 -> .5, infinity -> 1
	|

:END OF MACRO:
Initial Population=
	1
	~	people
	~		|

Initial Natural Capital=
	0.98
	~	NKU [0,2]
	~		|

Initial Output Per Cap=
	1
	~	$/(year*person)
	~		|

Initial Pollution Intensity=
	1
	~	PU/$
	~		|

********************************************************
	.Output
********************************************************~
	|

Contraction Rate=
	 0.1
	~	1/year [-1,0]
	~	eta
		Contraction rate when natural capital = 0
	|

Output Growth Rate=
	 Growth Rate-(Growth Rate+Contraction Rate)*(1-Natural Capital)^Contraction Nonlin
	~	1/year
	~		|

Contraction Nonlin = 2
	~	dmnl [0,4]
	~	lambda
	|

Growth Rate = 0.04
	~	1/year [0,0.1]
	~	gamma
		Growth rate when natural capital = 1
	|

Net per Capita Output = Output per Capita-Pollution Control
	~	$/person/year
	~	ybar
	|

Output Net Change Rate = Output per Capita*Output Growth Rate
	~	$/person/year/year
	~		|

Output per Capita= INTEG (
	Output Net Change Rate,
		Initial Output Per Cap)
	~	$/person/year
	~	y(t)
	|

********************************************************
	.NaturalCapital
********************************************************~
	|

Natural Capital= INTEG (
	Natural Capital Net Change Rate,
		Initial Natural Capital)
	~	NKU
	~	z(t)
	|

Natural Capital Net Change Rate=
	 Regeneration Rate*Natural Capital*(1-Natural Capital)
	~	NKU/year
	~		|

Cleansing Flow=
	 Cleansing Rate/Regen Coeff*Natural Capital^Cleansing Coeff
	~	PU/year
	~		|

Cleansing Coeff = 2
	~	dmnl
	~	rho
	|

Cleansing Rate = 1
	~	dmnl
	~	delta
	|

Normal Regen Rate = 1
	~	1/NKU/year
	~	nu
	|

Regen Coeff = 0.1
	~	1/PU*year
	~	omega
	|

Regeneration Rate = Normal Regen Rate*(P EXP(Regen Coeff*(Cleansing Flow-Net Pollution Flow\
		))-1)
	~	1/NKU/year
	~		|

********************************************************
	.Population
********************************************************~
	|

Envir Effect Death=
	 1+Envir Death Scale*MAX(0.01,1-Natural Capital)^Envir Death Nonlin
	~	dmnl
	~	MIN added to prevent floating point error.
	|

Birth Coeff 1 = 0.04
	~	1/year
	~	beta1
	|

Birth Coeff 2 = 1.375
	~	dmnl [0,4]
	~	beta2
	|

Birth Income Effect = 0.16
	~	1/$*person*year
	~	beta
	|

Birth Rate = Birth Coeff 1*(Birth Coeff 2-SSHAPE(Birth Income Effect*Net per Capita Output\
		))
	~	1/year
	~		|

Births = Birth Rate*Population
	~	people/year
	~		|

Death Coeff 1 = 0.01
	~	1/year
	~	delta1
	|

Death Coeff 2 = 2.5
	~	dmnl
	~	delta2
	|

Death Income Effect = 0.18
	~	1/$*person*year
	~	alpha
	|

Death Rate = Normal Death Rate*Envir Effect Death
	~	1/year
	~		|

Deaths = Population*Death Rate
	~	people/year
	~		|

Envir Death Nonlin = 15
	~	dmnl
	~	upsilon
	|

Envir Death Scale = 4
	~	dmnl
	~	delta3
	|

Normal Death Rate = Death Coeff 1*(Death Coeff 2-SSHAPE(Death Income Effect*Net per Capita Output\
		))
	~	1/year
	~		|

Population= INTEG (
	Births-Deaths,
		Initial Population)
	~	people
	~	x(t)
	|

********************************************************
	.Pollution
********************************************************~
	|

Pollution Control=
	 Control Scale*(1-Natural Capital)^Control Nonlin*Output per Capita
	~	$/person/year
	~	c(y,z)
	|

Abatement Coeff = 2
	~	PU/year
	~	kappa
	|

Control Effect = 0.02
	~	1/$*year
	~	epsilon
	|

Control Nonlin = 2
	~	dmnl
	~	mu
	|

Control Scale = 0.5
	~	dmnl
	~	j
	|

Net Pollution Flow = Pollution Output-Pollution Abatement
	~	PU/year
	~	f(x,y,z,p)
	|

Pollution Abatement = Abatement Coeff*(SSHAPE(Control Effect*Pollution Control*Population\
		)-0.5)
	~	PU/year
	~		|

Pollution Change Rate = -0.03
	~	1/year [-0.1,0.1]
	~	chi
	|

Pollution Intensity Change Rate = Pollution per Unit Output*Pollution Change Rate
	~	PU/$/year
	~	chi
	|

Pollution Output = Population*Output per Capita*Pollution per Unit Output
	~	PU/year
	~		|

Pollution per Unit Output= INTEG (
	Pollution Intensity Change Rate,
		Initial Pollution Intensity)
	~	PU/$
	~	p(t)
	|

********************************************************
	.Control
********************************************************~
		Simulation Control Paramaters
	|

FINAL TIME  = 300
	~	year
	~	The final time for the simulation.
	|

INITIAL TIME  = 0
	~	year
	~	The initial time for the simulation.
	|

SAVEPER  = 
        TIME STEP
	~	year
	~	The frequency with which output is stored.
	|

TIME STEP  = 0.5
	~	year
	~	The time step for the simulation.
	|

\\\---/// Sketch information - do not modify anything except names
V300  Do not put anything below this section - it will be ignored
*Structure
$192-192-192,0,Times New Roman|12||0-0-0|0-0-0|0-0-0|-1--1--1|-1--1--1|96,96,100,0
10,1,Population,516,67,40,20,3,3,0,2,0,0,0,0,-1--1--1,0-0-0,|12||128-0-255
12,2,48,699,66,10,8,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||128-0-255
11,3,48,645,67,5,8,34,3,0,0,1,0,0,0
10,4,Births,645,86,20,11,32,3,0,2,0,0,0,0,-1--1--1,0-0-0,|12||128-0-255
12,5,48,306,68,10,8,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||128-0-255
11,6,48,381,66,5,8,34,3,0,0,1,0,0,0
10,7,Deaths,381,85,23,11,32,3,0,2,0,0,0,0,-1--1--1,0-0-0,|12||128-0-255
10,8,Output per Capita,531,324,40,20,3,3,0,2,0,0,0,0,-1--1--1,0-0-0,|12||255-0-0
12,9,48,271,322,10,8,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||255-0-0
11,10,48,390,321,5,8,34,3,0,0,1,0,0,0
10,11,Output Net Change Rate,390,340,78,11,32,3,0,2,0,0,0,0,-1--1--1,0-0-0,|12||255-0-0
10,12,Natural Capital,411,501,40,20,3,3,0,2,0,0,0,0,-1--1--1,0-0-0,|12||0-128-0
12,13,48,691,502,10,8,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||0-128-0
11,14,48,585,501,5,8,34,3,0,0,1,0,0,0
10,15,Natural Capital Net Change Rate,585,524,103,11,32,3,0,2,0,0,0,0,-1--1--1,0-0-0,|12||0-128-0
10,16,Pollution per Unit Output,1032,274,40,20,3,3,0,2,0,0,0,0,-1--1--1,0-0-0,|12||128-64-0
12,17,48,1297,272,10,8,0,3,0,0,-1,0,0,0
11,18,48,1203,273,5,8,34,3,0,0,1,0,0,0
10,19,Pollution Intensity Change Rate,1203,300,56,19,40,3,0,2,0,0,0,0,-1--1--1,0-0-0,|12||128-64-0
10,20,Pollution Control,683,324,53,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||128-64-0
10,21,Net per Capita Output,598,245,71,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||255-0-0
10,22,Net Pollution Flow,910,393,59,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||128-64-0
10,23,Death Rate,237,129,36,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||128-0-255
10,24,Birth Rate,681,164,33,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||128-0-255
10,25,Cleansing Flow,623,581,49,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||0-128-0
10,26,Regeneration Rate,846,460,58,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||0-128-0
10,27,Normal Regen Rate,983,511,62,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||0-128-0
10,28,Regen Coeff,863,569,40,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||0-128-0
10,29,Cleansing Coeff,680,639,50,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||0-128-0
10,30,Cleansing Rate,550,639,47,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||0-128-0
10,31,Pollution Output,918,212,51,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||128-64-0
10,32,Pollution Abatement,844,320,63,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||128-64-0
10,33,Abatement Coeff,755,269,54,11,0,3,0,3,-1,0,0,0,128-128-128,0-0-0,|12||128-64-0
10,34,Control Effect,779,372,45,11,0,3,0,3,-1,0,0,0,128-128-128,0-0-0,|12||128-64-0
10,35,Control Nonlin,738,429,47,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||128-64-0
10,36,Control Scale,648,400,44,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||128-64-0
10,37,Output Growth Rate,400,380,64,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||255-0-0
10,38,Growth Rate,413,429,41,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||255-0-0
10,39,Contraction Rate,241,385,54,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||255-0-0
10,40,Contraction Nonlin,256,421,60,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||255-0-0
10,41,Envir Effect Death,176,252,57,11,0,3,0,3,-1,0,0,0,128-128-128,0-0-0,|12||128-0-255
10,42,Normal Death Rate,367,157,61,11,0,3,0,3,-1,0,0,0,128-128-128,0-0-0,|12||128-0-255
10,43,Death Coeff 1,282,214,45,11,0,3,0,3,-1,0,0,0,128-128-128,0-0-0,|12||128-0-255
10,44,Death Coeff 2,356,248,45,11,0,3,0,3,-1,0,0,0,128-128-128,0-0-0,|12||128-0-255
10,45,Death Income Effect,429,282,64,11,0,3,0,3,-1,0,0,0,128-128-128,0-0-0,|12||128-0-255
10,46,Envir Death Nonlin,70,305,60,11,0,3,0,3,-1,0,0,0,128-128-128,0-0-0,|12||128-0-255
10,47,Envir Death Scale,102,363,56,11,0,3,0,3,-1,0,0,0,128-128-128,0-0-0,|12||128-0-255
10,48,Birth Coeff 1,528,162,42,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||128-0-255
10,49,Birth Coeff 2,579,126,42,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||128-0-255
10,50,Birth Income Effect,567,202,61,11,0,3,0,2,-1,0,0,0,-1--1--1,0-0-0,|12||128-0-255
10,51,Pollution Change Rate,1236,363,69,11,0,3,0,3,-1,0,0,0,128-128-128,0-0-0,|12||128-64-0
1,52,3,1,4,0,0,22,0,0,0,-1--1--1,,1|(598,67)|
1,53,3,2,100,0,0,22,0,0,0,-1--1--1,,1|(669,67)|
1,54,6,5,4,0,0,22,0,0,0,-1--1--1,,1|(346,66)|
1,55,6,1,100,0,0,22,0,0,0,-1--1--1,,1|(431,66)|
1,56,10,8,4,0,0,22,0,0,0,-1--1--1,,1|(443,321)|
1,57,10,9,68,0,0,22,0,0,0,-1--1--1,,1|(333,321)|
1,58,14,12,4,0,0,22,0,0,0,-1--1--1,,1|(515,501)|
1,59,14,13,68,0,0,22,0,0,0,-1--1--1,,1|(635,501)|
1,60,18,16,4,0,0,22,0,0,0,-1--1--1,,1|(1135,273)|
1,61,18,17,68,0,0,22,0,0,0,-1--1--1,,1|(1247,273)|
1,62,8,20,0,0,0,0,0,0,0,-1--1--1,,1|(593,324)|
1,63,8,21,1,0,0,0,0,0,0,-1--1--1,,1|(553,273)|
1,64,20,21,1,0,0,0,0,0,0,-1--1--1,,1|(659,281)|
1,65,1,4,1,0,0,0,0,0,0,-1--1--1,,1|(581,99)|
1,66,1,7,1,0,0,0,0,0,0,-1--1--1,,1|(444,99)|
1,67,21,24,1,0,0,0,0,0,0,-1--1--1,,1|(645,216)|
1,68,24,4,1,0,0,0,0,0,0,-1--1--1,,1|(678,123)|
1,69,23,7,1,0,0,0,0,0,0,-1--1--1,,1|(308,93)|
1,70,22,26,1,0,0,0,0,0,0,-1--1--1,,1|(889,426)|
1,71,25,26,1,0,0,0,0,0,0,-1--1--1,,1|(766,543)|
1,72,26,15,1,0,0,0,0,0,0,-1--1--1,,1|(744,508)|
1,73,27,26,1,0,0,0,0,0,0,-1--1--1,,1|(950,478)|
1,74,28,26,1,0,0,0,0,0,0,-1--1--1,,1|(866,517)|
1,75,28,25,1,0,0,0,0,0,0,-1--1--1,,1|(749,590)|
1,76,29,25,0,0,0,0,0,0,0,-1--1--1,,1|(656,614)|
1,77,30,25,0,0,0,0,0,0,0,-1--1--1,,1|(580,614)|
1,78,1,31,1,0,0,0,0,0,0,-1--1--1,,1|(736,37)|
1,79,8,31,1,0,0,0,0,0,0,-1--1--1,,1|(720,250)|
1,80,16,31,1,0,0,0,0,0,0,-1--1--1,,1|(1008,232)|
1,81,31,22,1,0,0,0,0,0,0,-1--1--1,,1|(937,291)|
1,82,20,32,0,0,0,0,0,0,0,-1--1--1,,1|(751,322)|
1,83,32,22,1,0,0,0,0,0,0,-1--1--1,,1|(885,348)|
1,84,33,32,0,0,0,0,0,0,0,-1--1--1,,1|(792,290)|
1,85,34,32,0,0,0,0,0,0,0,-1--1--1,,1|(805,350)|
1,86,1,32,1,0,0,0,0,0,0,-1--1--1,,1|(808,130)|
1,87,35,20,0,0,0,0,0,0,0,-1--1--1,,1|(713,382)|
1,88,36,20,0,0,0,0,0,0,0,-1--1--1,,1|(662,368)|
1,89,37,11,0,0,0,0,0,0,0,-1--1--1,,1|(397,366)|
1,90,8,11,1,0,0,0,0,0,0,-1--1--1,,1|(485,358)|
1,91,39,37,0,0,0,0,0,0,0,-1--1--1,,1|(308,383)|
1,92,38,37,0,0,0,0,0,0,0,-1--1--1,,1|(408,411)|
1,93,40,37,0,0,0,0,0,0,0,-1--1--1,,1|(320,402)|
1,94,41,23,1,0,0,0,0,0,0,-1--1--1,,1|(192,202)|
1,95,42,23,1,0,0,0,0,0,0,-1--1--1,,1|(325,135)|
1,96,43,42,0,0,0,0,0,0,0,-1--1--1,,1|(318,189)|
1,97,44,42,0,0,0,0,0,0,0,-1--1--1,,1|(360,209)|
1,98,45,42,0,0,0,0,0,0,0,-1--1--1,,1|(401,225)|
1,99,21,42,1,0,0,0,0,0,0,-1--1--1,,1|(470,213)|
1,100,46,41,0,0,0,0,0,0,0,-1--1--1,,1|(116,281)|
1,101,47,41,0,0,0,0,0,0,0,-1--1--1,,1|(134,313)|
1,102,48,24,0,0,0,0,0,0,0,-1--1--1,,1|(602,162)|
1,103,49,24,0,0,0,0,0,0,0,-1--1--1,,1|(622,142)|
1,104,50,24,0,0,0,0,0,0,0,-1--1--1,,1|(617,185)|
1,105,16,19,1,0,0,0,0,0,0,-1--1--1,,1|(1087,314)|
1,106,51,19,0,0,0,0,0,0,0,-1--1--1,,1|(1224,341)|
1,107,12,15,1,0,0,0,0,64,0,-1--1--1,,1|(466,541)|
1,108,12,25,1,0,0,0,0,64,0,-1--1--1,,1|(527,579)|
1,109,12,41,1,0,0,0,0,64,0,-1--1--1,,1|(164,357)|
1,110,12,20,1,0,0,0,0,64,0,-1--1--1,,1|(507,425)|
1,111,12,37,1,0,0,0,0,64,0,-1--1--1,,1|(354,452)|
12,112,0,1094,47,156,11,0,4,0,0,-1,0,0,0
Wonderland Model by Warren C. Sanderson et al.
12,113,0,1121,125,183,11,0,4,0,0,-1,0,0,0
Replicated in Vensim by Tom Fiddaman, tom@metasd.com
12,114,0,1313,83,375,11,0,4,0,0,-1,0,0,0
From Alexandra Milik et al., "Slow-fast Dynamics in Wonderland," Environmental Modeling and Assessment 1 (1996) 3-17
10,115,Initial Population,514,21,52,11,8,3,1,0,-1,0,0,0
1,116,115,1,0,0,0,0,0,64,1,-1--1--1,,1|(514,32)|
12,117,0,1087,164,149,11,0,135,0,0,-1,0,0,0
Updated 2013, to make initial stocks parameters
10,118,Initial Pollution Intensity,1032,331,47,19,8,3,1,0,-1,0,0,0
1,119,118,16,0,0,0,0,0,64,1,-1--1--1,,1|(1032,310)|
10,120,Initial Natural Capital,407,540,43,19,8,3,1,0,-1,0,0,0
10,121,Initial Output Per Cap,529,380,55,19,8,3,1,0,-1,0,0,0
1,122,121,8,0,0,0,0,0,64,1,-1--1--1,,1|(529,359)|
1,123,120,12,0,0,0,0,0,64,1,-1--1--1,,1|(408,521)|
12,124,0,1443,164,186,12,8,135,0,2,-1,0,0,0,-1--1--1,0-0-0,|0||0-0-255
This version is intended for double-precision Vensim versions.
///---\\\
:GRAPH NATURAL_CAPITAL
:TITLE Natural Capital
:SCALE
:VAR Effective Natural Capital
:SCALE
:VAR Cleansing Flow
:VAR Net Pollution Flow
:SCALE
:VAR Regeneration Rate

:GRAPH Population
:TITLE Population
:SCALE
:VAR Population
:LINE-WIDTH 3
:SCALE
:VAR Birth Rate
:LINE-WIDTH 2
:VAR Normal Death Rate
:LINE-STYLE DASH
:VAR Death Rate
:LINE-WIDTH 2
:L<%^E!@
1:nightmareDP.vdf
1:dreamDP.vdf
1:baseDP.vdf
9:nightmareDP
22:$,Dollar,Dollars,$s
22:Day,Days
22:dmnl,NKU
22:Hour,Hours
22:Month,Months
22:Person,People,Persons
22:Unit,Units
22:Week,Weeks
22:Year,Years
15:0,0,0,1,0,0
19:100,0
27:2,
34:0,
4:Time
5:Natural Capital
24:0
25:300
26:300
